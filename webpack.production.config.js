const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

// eslint-disable-next-line immutable/no-mutation
module.exports = {
	entry: ["babel-polyfill", "./client/index.js"],
	output: {
		path: __dirname + "/dist/js",
		filename: "bundle.js"
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				loaders: ["babel-loader"],
				exclude: [/node_modules/]
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					use: ['css-loader', 'postcss-loader']
				})
			}]
	},
	plugins: [
		new webpack.LoaderOptionsPlugin({
			minimize: true,
			debug: false
		}),
		new ExtractTextPlugin("../css/styles.css"),
		new webpack.DefinePlugin({
			'process.env': {
				CLIENT: JSON.stringify(true),
				NODE_ENV: JSON.stringify("production")
			}
		}),
		new webpack.optimize.UglifyJsPlugin({
			minimize: true,
			compress: {
				warnings: false
			}
		}),
		new webpack.optimize.OccurrenceOrderPlugin()
	]
};
