// eslint-disable-next-line immutable/no-mutation
module.exports = {
	apps: [
		{
			name: "aksit",
			script: "./server/server.js",
			watch: false,
			env: {
				NODE_ENV: "staging",
				PORT: 4000
			},
			env_production: {
				NODE_ENV: "production"
			}
		}
	]
};
