const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CircularDependencyPlugin = require('circular-dependency-plugin');

// eslint-disable-next-line immutable/no-mutation
module.exports = {
	entry: ["babel-polyfill", "./client/index.js"],
	output: {
		path: __dirname + "/dist/js",
		filename: "bundle.js"
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				loaders: ["babel-loader"],
				exclude: [/node_modules/]
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					use: ['css-loader', 'postcss-loader']
				})
			}]
	},
	plugins: [
		new ExtractTextPlugin("../css/styles.css"),
		new webpack.DefinePlugin({
			'process.env': {
				CLIENT: JSON.stringify(true),
				NODE_ENV: JSON.stringify(process.env.NODE_ENV || "staging")
			}
		}),
		new CircularDependencyPlugin({
			// exclude detection of files based on a RegExp 
			exclude: /node_modules/,
			// add errors to webpack instead of warnings 
			failOnError: true
		})
	]
};
