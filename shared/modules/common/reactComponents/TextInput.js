// @flow
"use strict";
import React from "react";
import {
	validateText
} from "../../../helpers/validations/validations";


type FirstNameInputPropsType = {
	fieldName: string,
	value: string,
	onChange: (string, string) => void,
	error: ?string,
	setError: (string, string) => void,
	removeError: (string) => Promise<void>,
	title: string,
	required: boolean,
	minLength: number,
	maxLength: number
};
class FirstNameInput extends React.Component {
	props: FirstNameInputPropsType;
	onChange = (e: SyntheticInputEvent) => {
		const newValue = e.target.value;
		const {minLength, maxLength, required} = this.props;
		if (required && !newValue) {
			this.props.setError(
				this.props.fieldName,
				"This field is required"
			);
		}
		else if (newValue && !validateText(newValue, minLength, maxLength)) {
			this.props.setError(
				this.props.fieldName,
				`Please enter valid value`
			);
		}
		else {
			this.props.removeError(this.props.fieldName);
		}
		this.props.onChange(this.props.fieldName, newValue);
	};
	render() {
		const {
			value,
			error,
			title
		} = this.props;
		return (
			<div className="input-field text-input">
				<h4>{title}</h4>
				<div className="input-group">
					<input
						type="text"
						className="form-control"
						placeholder=""
						onChange={this.onChange}
						value={value} />
					{
						error &&
						<p className="form-error-message">
							{this.props.error}
						</p>
					}
				</div>
			</div>);
	}
}

export default FirstNameInput;
