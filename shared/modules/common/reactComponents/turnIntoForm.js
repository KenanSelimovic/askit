// @flow
"use strict";
import React from "react";

// flow types
type FormComponentStateType = {
	errors: {[key: string]: ?string},
	defaultErrors: {[key: string]: ?string}
};

export default (ComponentToWrap: any) => {
	class FormComponent extends React.PureComponent {
		state: FormComponentStateType = {
			errors: {},
			defaultErrors: {}
		}
		addTrackedField = (name: string, required: boolean = true, alreadyValid: boolean = false) => {
			if (this.state.errors[name]) {
				return;
			}
			this.setState((prevState: FormComponentStateType) => ({
				errors: Object.assign(
					{},
					prevState.errors,
					{
						[name]: required && !alreadyValid ? "" : null
					}
				),
				defaultErrors: Object.assign(
					{},
					prevState.defaultErrors,
					{
						[name]: required && !alreadyValid ? "" : null
					}
				)
			}));
		};
		setError = (field: string, message: string): Promise<void> => {
			return new Promise(resolve => {
				const newErrorsState = Object.assign(
					{},
					this.state.errors,
					{
						[field]: message
					}
				);
				this.setState({errors: newErrorsState}, () => resolve());
			});
		};
		resetAllErrors = (): Promise<void> => {
			return new Promise(resolve => {
				this.setState((prevState) => {
					return Object.assign(
						{},
						prevState,
						{
							errors: {}
						}
					);
				}, () => resolve());
			});
		};
		removeError = (field: string): Promise<void> => {
			return new Promise(resolve => {
				this.setState((prevState) => {
					return Object.assign(
						{},
						prevState,
						{
							errors: Object.assign(
								{},
								prevState.errors,
								{
									[field]: null
								}
							)
						}
					);
				}, () => resolve());
			});
		};
		triggerRequiredFieldError = (field: string) => {
			this.setError(field, "This field is required");
		}
		checkIfAnyErrors = (fieldsToCheck: Array<string>): boolean => {
			/*
			 * This one is called on user pressing next.
			 * When that happens, we go through all fields and
			 * check if there are any errors for them.
			 * In case where field is mandatory, but no error is
			 * set because the user hasn't actuall changed the field,
			 * we will set error to empty string
			 */
			const fields
				= Object
					.keys(this.state.errors)
					.filter(
						f => fieldsToCheck.indexOf(f) !== -1
					);
			for (const field of fields) {
				const currentFieldError = this.state.errors[field];
				if (currentFieldError !== null) {
					if (currentFieldError === "") {
						this.triggerRequiredFieldError(field);
					}
					return true;
				}
			}
			return false;
		}
		render() {
			return (
				<ComponentToWrap
					{...this.props}
					checkIfAnyErrors={this.checkIfAnyErrors}
					triggerRequiredFieldError={this.triggerRequiredFieldError}
					setError={this.setError}
					removeError={this.removeError}
					resetAllErrors={this.resetAllErrors}
					errors={this.state.errors}
					addTrackedField={this.addTrackedField} />);
		}
	}
	return FormComponent;
};
