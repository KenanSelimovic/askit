// @flow
"use strict";
import React from "react";
import {
	validatePassword
} from "../../../helpers/validations/validations";


type PasswordInputPropsType = {
	fieldName: string,
	value: string,
	onChange: (string, string) => void,
	error: ?string,
	setError: (string, string) => void,
	removeError: (string) => Promise<void>,
	title: string,
	required: boolean,
	minLength: number,
	maxLength: number,
	repeatedValue?: string
};
class PasswordInput extends React.Component {
	props: PasswordInputPropsType;
	onChange = (e: SyntheticInputEvent) => {
		const newValue = e.target.value;
		const {
			minLength,
			maxLength,
			required,
			repeatedValue
		} = this.props;
		if (required && !newValue) {
			this.props.setError(
				this.props.fieldName,
				"This field is required"
			);
		}
		else if (!validatePassword(newValue, minLength, maxLength)) {
			this.props.setError(
				this.props.fieldName,
				`Please enter a longer password`
			);
		}
		else {
			this.props.removeError(this.props.fieldName)
				.then(() => {
					if (typeof repeatedValue !== "undefined") {
						if (repeatedValue !== newValue) {
							this.props.setError(
								"password_second_time",
								"Passwords do not match"
							);
						}
						else {
							this.props.removeError("password_second_time");
						}
					}
				});
		}
		this.props.onChange(this.props.fieldName, newValue);
	};
	render() {
		const {
			value,
			error,
			title
		} = this.props;
		return (
			<div className="input-field password-input">
				<h4>{title}</h4>
				<div className="input-group">
					<input
						type="password"
						className="form-control"
						placeholder=""
						onChange={this.onChange}
						value={value} />
					{
						error &&
						<p className="form-error-message">
							{this.props.error}
						</p>
					}
				</div>
			</div>);
	}
}

export default PasswordInput;
