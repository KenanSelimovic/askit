// @flow
"use strict";
import React from "react";

if (process.env.CLIENT) {
	require("../styles/voteIcon.css");
}

import type {VoteType} from "../../votes/";
type VoteIconPropsType = {
	type: VoteType,
	numVotes: number,
	active: boolean,
	onClick: () => void
};

const VoteIcon = (props: VoteIconPropsType) => {
	const iconClass
		= props.type === "up"
			? "glyphicon-thumbs-up"
			: "glyphicon-thumbs-down";
	const activeClass = props.active ? "active" : "";
	const fullClass = `glyphicon ${iconClass} ${activeClass}`;
	return (
		<div className="vote-icon">
			<span
				className={fullClass}
				onClick={props.onClick} />
			{props.numVotes}
		</div>
	);
};
export default VoteIcon;
