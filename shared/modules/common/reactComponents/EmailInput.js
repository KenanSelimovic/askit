// @flow
"use strict";
import React from "react";
import {
	validateEmail
} from "../../../helpers/validations/validations";


type EmailInputPropsType = {
	fieldName: string,
	value: string,
	onChange: (string, string) => void,
	error: ?string,
	setError: (string, string) => void,
	removeError: (string) => Promise<void>
};
class EmailInput extends React.Component {
	props: EmailInputPropsType;
	onChange = (e: SyntheticInputEvent) => {
		const newEmail = e.target.value;
		if (!validateEmail(newEmail)) {
			this.props.setError(
				this.props.fieldName,
				"Please enter valid email"
			);
		}
		else {
			this.props.removeError(this.props.fieldName);
		}
		this.props.onChange(this.props.fieldName, newEmail);
	};
	render() {
		const {
			value,
			error
		} = this.props;
		return (
			<div className="input-field email-input">
				<h4>Email</h4>
				<div className="input-group">
					<input
						type="text"
						className="form-control"
						placeholder=""
						onChange={this.onChange}
						value={value} />
					{
						error &&
						<p className="form-error-message">
							{this.props.error}
						</p>
					}
				</div>
			</div>);
	}
}

export default EmailInput;
