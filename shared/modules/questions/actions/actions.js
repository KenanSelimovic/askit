// @flow
"use strict";
import * as actionTypes from "./types";
import {baseUrl} from "../../../config/";
import toastr from "toastr";
import logError from "../../../../client/modules/logging/errorLogger";
import {config} from "../../../config/index";
import fetchHotQuestions from "../methods/fetchHotQuestions";
import fetchLatestQuestions from "../methods/fetchLatestQuestions";
import fetchMyQuestions from "../methods/fetchMyQuestions";
import submitQuestionsSearch from "../methods/submitQuestionsSearch";
import fetchSingleQuestion from "../methods/fetchSingleQuestion";

import type {GlobalAppStateType} from "../../../redux/defaultState";
import type {
	QuestionsActionType
} from "./actions.flow";
import type {
	TopUsersResponseType
} from "../flowTypes/apiResponses.flow";
import type {
	QuestionType,
	UserStatisticsType
} from "../flowTypes/questions.flow";
import type {Dispatch} from "redux";

const PER_PAGE = config.questions.questions_per_page;

export const questionsLoadSuccess = (
	questions: Array<QuestionType>,
	page: number,
	totalPages: number
) => ({
	type: actionTypes.QUESTIONS_LOAD_SUCCESS,
	data: {
		questions,
		page,
		perPage: PER_PAGE,
		totalPages
	}
});
export const hotQuestionsLoadSuccess = (
	questions: Array<QuestionType>
) => ({
	type: actionTypes.HOT_QUESTIONS_LOAD_SUCCESS,
	data: {
		questions
	}
});
export const myQuestionsLoadSuccess = (
	questions: Array<QuestionType>,
	page: number,
	totalPages: number
) => ({
	type: actionTypes.MY_QUESTIONS_LOAD_SUCCESS,
	data: {
		questions,
		page,
		perPage: PER_PAGE,
		totalPages
	}
});
export const searchQuestionsLoadSuccess = (
	questions: Array<QuestionType>,
	page: number,
	totalPages: number,
	text: string
) => ({
	type: actionTypes.SEARCH_QUESTIONS_LOAD_SUCCESS,
	data: {
		questions,
		page,
		perPage: PER_PAGE,
		totalPages,
		searchText: text
	}
});
export const topUsersLoadSuccess = (
	users: Array<UserStatisticsType>
) => ({
	type: actionTypes.TOP_USERS_LOAD_SUCCESS,
	data: {
		users
	}
});
export const questionLoadSuccess = (
	question: QuestionType
) => ({
	type: actionTypes.QUESTION_LOAD_SUCCESS,
	data: {
		question
	}
});

export const loadHotQuestions = () => {
	return (dispatch: Dispatch<QuestionsActionType>) => {
		fetchHotQuestions()
			.then((hotQuestions: Array<QuestionType>) => {
				dispatch(hotQuestionsLoadSuccess(
					hotQuestions
				));
			})
			.catch(err => {
				logError(
					new Error(
						`Failed retrieving hot questions: ${JSON.stringify(err)}`
					)
				);
				toastr.error("Unknown error occurred");
			});
	};
};
export const loadLatestQuestions = (page: number) => {
	return (dispatch: Dispatch<QuestionsActionType>) => {
		fetchLatestQuestions(page)
			.then((latestQuestionsData) => {
				dispatch(questionsLoadSuccess(
					latestQuestionsData.questions,
					latestQuestionsData.page,
					latestQuestionsData.total_pages
				));
			})
			.catch(err => {
				logError(
					new Error(
						`Failed retrieving latest questions: ${JSON.stringify(err)}`
					)
				);
				toastr.error("Unknown error occurred");
			});
	};
};
export const loadMyQuestions = (page: number) => {
	return (dispatch: Dispatch<QuestionsActionType>, getState: () => GlobalAppStateType) => {
		const token = getState().auth.token;
		if (!token) {
			return;
		}
		fetchMyQuestions(page, token)
			.then((myQuestionsData) => {
				dispatch(myQuestionsLoadSuccess(
					myQuestionsData.questions,
					myQuestionsData.page,
					myQuestionsData.total_pages
				));
			})
			.catch(err => {
				logError(
					new Error(
						`Failed retrieving own questions: ${JSON.stringify(err)}`
					)
				);
				toastr.error("Unknown error occurred");
			});
	};
};
export const searchQuestions = (text: string, page: number) => {
	return (dispatch: Dispatch<QuestionsActionType>) => {
		submitQuestionsSearch(page, text)
			.then((searchData) => {
				dispatch(searchQuestionsLoadSuccess(
					searchData.questions,
					searchData.page,
					searchData.total_pages,
					text
				));
			})
			.catch(err => {
				logError(
					new Error(
						`Failed retrieving search questions: ${JSON.stringify(err)}`
					)
				);
				toastr.error("Unknown error occurred");
			});
	};
};
export const loadSingleQuestion = (id: number) => {
	return (dispatch: Dispatch<QuestionsActionType>) => {
		fetchSingleQuestion(id)
			.then((question: ?QuestionType) => {
				if (question) {
					dispatch(questionLoadSuccess(
						question
					));
				}
			})
			.catch(err => {
				logError(
					new Error(
						`Failed retrieving question ${id}: ${JSON.stringify(err)}`
					)
				);
				toastr.error("Unknown error occurred");
			});
	};
};
export const loadTopUsers = () => {
	return (dispatch: Dispatch<QuestionsActionType>) => {
		const fetchUrl = `${baseUrl}/v1/users/top`;
		fetch(fetchUrl)
			.then(unparsed => unparsed.json())
			.then((usersResponse: TopUsersResponseType) => {
				if (usersResponse.status !== 200 || !usersResponse.data) {
					logError(
						new Error(
							`Failed fetching top users: ${JSON.stringify(usersResponse)}`
						)
					);
					toastr.error("Unknown error occurred");
					return;
				}
				dispatch(
					topUsersLoadSuccess(
						usersResponse.data.users
					)
				);
			})
			.catch(err => {
				logError(err);
				toastr.error("Unknown error occurred");
			});
	};
};
