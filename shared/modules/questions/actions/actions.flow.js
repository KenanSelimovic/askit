// @flow
"use strict";
import type {
	QuestionType,
	UserStatisticsType
} from "../flowTypes/questions.flow";

export type QuestionsLoadSuccessActionType = {|
	type: "QUESTIONS_LOAD_SUCCESS",
	data: {
		questions: Array<QuestionType>,
		page: number,
		perPage: number,
		totalPages: number
	}
|};
export type MyQuestionsLoadSuccessActionType = {|
	type: "MY_QUESTIONS_LOAD_SUCCESS",
	data: {
		questions: Array<QuestionType>,
		page: number,
		perPage: number,
		totalPages: number
	}
|};
export type SearchQuestionsLoadSuccessActionType = {|
	type: "SEARCH_QUESTIONS_LOAD_SUCCESS",
	data: {
		questions: Array<QuestionType>,
		searchText: string,
		page: number,
		perPage: number,
		totalPages: number
	}
|};
export type HotQuestionsLoadSuccessActionType = {|
	type: "HOT_QUESTIONS_LOAD_SUCCESS",
	data: {
		questions: Array<QuestionType>
	}
|};
export type TopUsersLoadSuccessActionType = {|
	type: "TOP_USERS_LOAD_SUCCESS",
	data: {
		users: Array<UserStatisticsType>
	}
|};
export type QuestionLoadSuccessActionType = {|
	type: "QUESTION_LOAD_SUCCESS",
	data: {
		question: QuestionType
	}
|};
export type QuestionsActionType =
	QuestionsLoadSuccessActionType
	| QuestionLoadSuccessActionType
	| HotQuestionsLoadSuccessActionType
	| TopUsersLoadSuccessActionType
	| MyQuestionsLoadSuccessActionType
	| SearchQuestionsLoadSuccessActionType;
