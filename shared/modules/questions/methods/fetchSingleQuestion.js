// @flow
"use strict";
import {baseUrl} from "../../../config/";

import type {
	QuestionResponseType
} from "../flowTypes/apiResponses.flow";


export default (id: number) => {
	return new Promise((resolve, reject) => {
		const fetchUrl = `${baseUrl}/v1/questions/${id}`;
		return fetch(fetchUrl)
			.then(unparsed => unparsed.json())
			.then((questionResponse: QuestionResponseType) => {
				if (!questionResponse.data) {
					reject(questionResponse);
					return;
				}
				resolve(questionResponse.data.question);
			})
			.catch(err => {
				reject(err);
			});
	});
};

