// @flow
"use strict";
import {baseUrl} from "../../../config/";
import {config} from "../../../config/index";

import type {
	LatestQuestionsResponseType
} from "../flowTypes/apiResponses.flow";

const PER_PAGE = config.questions.questions_per_page;

export default (page: number) => {
	return new Promise((resolve, reject) => {
		const fetchUrl = `${baseUrl}/v1/questions?page=${page}&per_page=${PER_PAGE}`;
		return fetch(fetchUrl)
			.then(unparsed => unparsed.json())
			.then((questionsResponse: LatestQuestionsResponseType) => {
				if (!questionsResponse.data) {
					reject(questionsResponse);
					return;
				}
				resolve(questionsResponse.data);
			})
			.catch(err => {
				reject(err);
			});
	});
};
