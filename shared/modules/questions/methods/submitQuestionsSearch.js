// @flow
"use strict";
import {baseUrl} from "../../../config/";
import {config} from "../../../config/index";

import type {
	SearchQuestionsResponseType
} from "../flowTypes/apiResponses.flow";

const PER_PAGE = config.questions.questions_per_page;

export default (page: number, text: string) => {
	return new Promise((resolve, reject) => {
		const fetchUrl
			= `${baseUrl}/v1/questions/search?page=${page}&per_page=${PER_PAGE}&text=${text}`;
		fetch(fetchUrl)
			.then(unparsed => unparsed.json())
			.then((questionsResponse: SearchQuestionsResponseType) => {
				if (!questionsResponse.data) {
					reject(questionsResponse);
					return;
				}
				resolve(questionsResponse.data);
			})
			.catch(err => {
				reject(err);
			});
	});
};
