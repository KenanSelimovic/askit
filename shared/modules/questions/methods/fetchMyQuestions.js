// @flow
"use strict";
import {baseUrl} from "../../../config/";
import {config} from "../../../config/index";

import type {
	MyQuestionsResponseType
} from "../flowTypes/apiResponses.flow";

const PER_PAGE = config.questions.questions_per_page;

export default (page: number, token: string) => {
	return new Promise((resolve, reject) => {
		const fetchUrl = `${baseUrl}/v1/questions/own?page=${page}&per_page=${PER_PAGE}`;
		fetch(fetchUrl, {
			headers: {
				authorization: token
			}
		})
			.then(unparsed => unparsed.json())
			.then((questionsResponse: MyQuestionsResponseType) => {
				if (!questionsResponse.data) {
					reject(questionsResponse);
					return;
				}
				resolve(questionsResponse.data);
			})
			.catch(err => {
				reject(err);
			});
	});
};
