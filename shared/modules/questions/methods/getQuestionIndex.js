// @flow
"use strict";
import type {QuestionType} from "../flowTypes/questions.flow";

export default (questions: Array<QuestionType>, questionId: number) => {
	return (questions.findIndex(question => question.id === questionId));
};
