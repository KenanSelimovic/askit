// @flow
"use strict";
import {baseUrl} from "../../../config/";

import type {
	HotQuestionsResponseType
} from "../flowTypes/apiResponses.flow";


export default () => {
	return new Promise((resolve, reject) => {
		const fetchUrl = `${baseUrl}/v1/questions/hot`;
		return fetch(fetchUrl)
			.then(unparsed => unparsed.json())
			.then((questionsResponse: HotQuestionsResponseType) => {
				if (!questionsResponse.data) {
					reject(questionsResponse);
					return;
				}
				resolve(questionsResponse.data.questions);
			})
			.catch(err => {
				reject(err);
			});
	});
};

