// @flow
"use strict";
import getQuestionIndex from "./getQuestionIndex";

import type {QuestionType} from "../flowTypes/questions.flow";

export default (questions: Array<QuestionType>, questionId: number) => {
	return getQuestionIndex(questions, questionId) !== -1;
};
