// @flow
"use strict";
import type {VoteDataType, VoteType} from "../../votes/";

export default (questionId: number, votes: Array<VoteDataType>): ?VoteType => {
	const userVote
		= votes
			.find(vote => vote.post_id === questionId);
	return userVote ? userVote.type : null;
};
