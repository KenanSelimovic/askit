// @flow
"use strict";
import questionsReducer from "../reducers/questionsReducer";
import test from "tape";
import * as actionTypes from "../actions/types";

test("properly inserts elements if first page loading", (t: tape$Context) => {
	const initialState = {
		latestQuestions: {
			data: [],
			totalPages: 1
		},
		hotQuestions: {
			data: []
		}
	};
	const newQuestions = [
		{
			user: {
				id: 748,
				full_name: "test user",
				profile_image_url: null
			},
			text: "pitanje 13",
			id: 160,
			time: 1500489738193,
			votes: {
				up: 1,
				down: 0
			},
			answers: []
		},
		{
			user: {
				id: 748,
				full_name: "test user",
				profile_image_url: null
			},
			text: "pitanje 12",
			id: 159,
			time: 1500489657822,
			votes: {
				up: 0,
				down: 1
			},
			answers: []
		},
		{
			user: {
				id: 745,
				full_name: "",
				profile_image_url: "/images/c0148b03c156b7a1398720f566ef07f8.png"
			},
			text: "pitanje 11",
			id: 155,
			time: 1500447939993,
			votes: {
				up: 0,
				down: 2
			},
			answers: []
		},
		{
			user: {
				id: 745,
				full_name: "",
				profile_image_url: "/images/c0148b03c156b7a1398720f566ef07f8.png"
			},
			text: "pitanje 10",
			id: 154,
			time: 1500447905577,
			votes: {
				up: 1,
				down: 1
			},
			answers: []
		},
		{
			user: {
				id: 745,
				full_name: "",
				profile_image_url: "/images/c0148b03c156b7a1398720f566ef07f8.png"
			},
			text: "pitanje 9",
			id: 153,
			time: 1500447681322,
			votes: {
				up: 1,
				down: 1
			},
			answers: []
		}
	];
	const action = {
		type: actionTypes.QUESTIONS_LOAD_SUCCESS,
		data: {
			questions: newQuestions,
			page: 1,
			perPage: 5,
			totalPages: 10
		}
	};
	const expectedState = {
		latestQuestions: {
			data: newQuestions,
			totalPages: 10
		},
		hotQuestions: {
			data: []
		}
	};
	const newState = questionsReducer(initialState, action);
	t.deepEqual(newState, expectedState, "Questions are added properly");
	t.end();
});
test("properly inserts new elements if second page", (t: tape$Context) => {
	const initialQuestions = [
		{
			user: {
				id: 748,
				full_name: "test user",
				profile_image_url: null
			},
			text: "pitanje 1",
			id: 160,
			time: 1500489738193,
			votes: {
				up: 1,
				down: 0
			},
			answers: []
		},
		{
			user: {
				id: 748,
				full_name: "test user",
				profile_image_url: null
			},
			text: "pitanje 2",
			id: 159,
			time: 1500489657822,
			votes: {
				up: 0,
				down: 1
			},
			answers: []
		},
		{
			user: {
				id: 745,
				full_name: "",
				profile_image_url: "/images/c0148b03c156b7a1398720f566ef07f8.png"
			},
			text: "pitanje 3",
			id: 155,
			time: 1500447939993,
			votes: {
				up: 0,
				down: 2
			},
			answers: []
		},
		{
			user: {
				id: 745,
				full_name: "",
				profile_image_url: "/images/c0148b03c156b7a1398720f566ef07f8.png"
			},
			text: "pitanje 4",
			id: 154,
			time: 1500447905577,
			votes: {
				up: 1,
				down: 1
			},
			answers: []
		},
		{
			user: {
				id: 745,
				full_name: "",
				profile_image_url: "/images/c0148b03c156b7a1398720f566ef07f8.png"
			},
			text: "pitanje 5",
			id: 153,
			time: 1500447681322,
			votes: {
				up: 1,
				down: 1
			},
			answers: []
		}
	];
	const initialState = {
		latestQuestions: {
			data: initialQuestions,
			totalPages: 1
		},
		hotQuestions: {
			data: []
		}
	};
	const newQuestions = [
		{
			user: {
				id: 748,
				full_name: "test user",
				profile_image_url: null
			},
			text: "pitanje 6",
			id: 100,
			time: 1500489738193,
			votes: {
				up: 1,
				down: 0
			},
			answers: []
		},
		{
			user: {
				id: 748,
				full_name: "test user",
				profile_image_url: null
			},
			text: "pitanje 7",
			id: 99,
			time: 1500489657822,
			votes: {
				up: 0,
				down: 1
			},
			answers: []
		},
		{
			user: {
				id: 745,
				full_name: "",
				profile_image_url: "/images/c0148b03c156b7a1398720f566ef07f8.png"
			},
			text: "pitanje 8",
			id: 98,
			time: 1500447939993,
			votes: {
				up: 0,
				down: 2
			},
			answers: []
		},
		{
			user: {
				id: 745,
				full_name: "",
				profile_image_url: "/images/c0148b03c156b7a1398720f566ef07f8.png"
			},
			text: "pitanje 9",
			id: 97,
			time: 1500447905577,
			votes: {
				up: 1,
				down: 1
			},
			answers: []
		},
		{
			user: {
				id: 745,
				full_name: "",
				profile_image_url: "/images/c0148b03c156b7a1398720f566ef07f8.png"
			},
			text: "pitanje 10",
			id: 96,
			time: 1500447681322,
			votes: {
				up: 1,
				down: 1
			},
			answers: []
		}
	];
	const action = {
		type: actionTypes.QUESTIONS_LOAD_SUCCESS,
		data: {
			questions: newQuestions,
			page: 2,
			perPage: 5,
			totalPages: 10
		}
	};
	const expectedState = {
		latestQuestions: {
			data: [...initialQuestions, ...newQuestions],
			totalPages: 10
		},
		hotQuestions: {
			data: []
		}
	};
	const newState = questionsReducer(initialState, action);
	t.deepEqual(newState, expectedState, "Questions are added properly");
	t.end();
});
