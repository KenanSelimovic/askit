// @flow
"use strict";
import type {QuestionType} from "./questions.flow";
import type {UserStatisticsType} from "./questions.flow";

export type QuestionsWithPaginationResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		questions: Array<QuestionType>,
		total_pages: number,
		page: number
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};
export type SubmitQuestionResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		id: number
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};
export type QuestionVotingResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: null
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};
export type QuestionResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		question: ?QuestionType
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};
export type QuestionsWithoutPaginationResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		questions: Array<QuestionType>
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};
export type TopUsersResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		users: Array<UserStatisticsType>
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};

export type LatestQuestionsResponseType = QuestionsWithPaginationResponseType;
export type SearchQuestionsResponseType = QuestionsWithPaginationResponseType;
export type MyQuestionsResponseType = QuestionsWithPaginationResponseType;

export type HotQuestionsResponseType = QuestionsWithoutPaginationResponseType;
