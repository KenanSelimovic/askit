// @flow
"use strict";
import type {AnswerType} from "../../answers/";

export type QuestionType = {|
	id: number,
	user: {
		id: number,
		full_name: ?string,
		profile_image_url: ?string
	},
	text: string,
	votes: {
		up: number,
		down: number
	},
	answers: Array<AnswerType>,
	time: number
|};
export type UserStatisticsType = {|
	id: number,
	full_name: string,
	profile_image_url: ?string,
	num_answers: number,
	num_questions: number,
	num_likes: number
|};
