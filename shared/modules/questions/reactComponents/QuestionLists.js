// @flow
"use strict";
import React from "react";
import LatestQuestions from "./LatestQuestions";
import HotQuestions from "./HotQuestions";
import QuestionsNav from "./QuestionsNav";
import TopUsers from "./TopUsers";
import Search from "./Search";

type QuestionListsStateType = {
	currentView: number
};

class QuestionLists extends React.PureComponent {
	state: QuestionListsStateType = {
		currentView: 1
	};
	changeView = (newView: number) => {
		this.setState(() => ({
			currentView: newView
		}));
	};
	getCurrentList = () => {
		const {currentView} = this.state;
		switch (currentView) {
		case 2:
			return TopUsers;
		case 3:
			return HotQuestions;
		case 4:
			return Search;
		default:
			return LatestQuestions;
		}
	};
	render() {
		const CurrentQuestionList = this.getCurrentList();
		return (
			<div>
				<QuestionsNav
					changeView={this.changeView} />
				<CurrentQuestionList />
			</div>
		);
	}
}
export default QuestionLists;
