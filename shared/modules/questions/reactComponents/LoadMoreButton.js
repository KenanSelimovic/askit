// @flow
"use strict";
import React from "react";

if (process.env.CLIENT) {
	require("../styles/loadMoreButton.css");
}

type LoadMoreButtonPropsType = {
	action: () => void
};

const LoadMoreButton = (props: LoadMoreButtonPropsType) => {
	return (
		<div className="load-more-button">
			<button className="btn btn-default askit-blue-button" onClick={props.action}>
				Load more
			</button>
		</div>
	);
};
export default LoadMoreButton;
