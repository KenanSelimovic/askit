// @flow
"use strict";
import React from "react";
import QuestionsList from "./QuestionsList";
import * as questionsActions from "../actions/actions";
import {connect} from "react-redux";
import {
	getQuestions,
	getTotalPages
} from "../selectors/questionSelectors";
import {
	selectors as votesSelectors
} from "../../votes/";
import type {QuestionType} from "../flowTypes/questions.flow";
import LoadMoreButton from "./LoadMoreButton";
import {selectors as authSelectors} from "../../auth/";
import AskQuestion from "./AskQuestion";
import getUserVoteForQuestion from "../methods/getUserVoteForQuestion";
import {config} from "../../../config/index";

const PER_PAGE = config.questions.questions_per_page;

import type {GlobalAppStateType} from "../../../redux/defaultState";
import type {Dispatch} from "redux";
import type {VoteDataType, VoteType} from "../../votes/";
type LatestQuestionsStateType = {
	page: number
};
type LatestQuestionsPropsType = {
	questions: Array<QuestionType>,
	totalPages: number,
	loadQuestions: (page: number) => void,
	isAuthenticated: boolean,
	userVotes: Array<VoteDataType>
};

class LatestQuestions extends React.PureComponent {
	props: LatestQuestionsPropsType;
	state: LatestQuestionsStateType = {
		page: 1
	};
	componentDidMount = () => {
		this.props.loadQuestions(1);
	};
	loadMore = () => {
		this.setState((prevState: LatestQuestionsStateType) => ({
			page: prevState.page + 1
		}), this.fetchIfNeeded);
	};
	fetchIfNeeded = () => {
		const {page} = this.state;
		if (!this.pageIsLoaded(page)) {
			this.props.loadQuestions(page);
		}
		const nextPage = page + 1;
		if (nextPage <= this.props.totalPages) {
			if (!this.pageIsLoaded(nextPage)) {
				this.props.loadQuestions(nextPage);
			}
		}
	};
	getUserVoteForQuestion = (questionId: number): ?VoteType => {
		return getUserVoteForQuestion(questionId, this.props.userVotes);
	};
	pageIsLoaded = (page: number) => {
		const sliceStart = page * PER_PAGE - PER_PAGE;
		const sliceEnd = sliceStart + PER_PAGE;
		return this.props.questions.slice(sliceStart, sliceEnd).length !== 0;
	}
	getCurrentQuestions = (page: number = this.state.page) => {
		const sliceEnd = page * PER_PAGE;
		return this.props.questions.slice(0, sliceEnd);
	}
	render() {
		const {
			totalPages,
			isAuthenticated
		} = this.props;
		const {
			page
		} = this.state;
		const questionsData = this.getCurrentQuestions();
		return (
			<div className="latest-questions container ">
				{isAuthenticated &&
				<AskQuestion />}
				<QuestionsList
					data={questionsData}
					getUserVote={this.getUserVoteForQuestion}
					title="Latest questions" />
				{questionsData.length !== 0 && page !== totalPages &&
				<LoadMoreButton
					action={this.loadMore} />}
			</div>
		);
	}
}
const mapStateToProps = (state: GlobalAppStateType) => {
	return ({
		questions: getQuestions(state),
		totalPages: getTotalPages(state),
		isAuthenticated: authSelectors.getLoginStatus(state),
		userVotes: votesSelectors.getUserVotes(state)
	});
};
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	loadQuestions: (page: number = 1) => {
		dispatch(questionsActions.loadLatestQuestions(page));
	}
});
export default connect(mapStateToProps, mapDispatchToProps)(LatestQuestions);
