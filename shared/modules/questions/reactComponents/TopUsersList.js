// @flow
"use strict";
import React from "react";
import UserStatistics from "./UserStatistics";

if (process.env.CLIENT) {
	require("../styles/questionsList.css");
}

import type {UserStatisticsType} from "../flowTypes/questions.flow";
type UsersListPropsType = {|
	data: Array<UserStatisticsType>
|};

const UsersList = (props: UsersListPropsType) => {
	const generatedUsers = props.data.map((user, index) => {
		return (
			<UserStatistics
				key={index}
				data={user} />
		);
	});
	return (
		<div className="questions-list">
			<h3 className="askit-blue">Most active users</h3>
			{generatedUsers}
		</div>
	);
};
export default UsersList;
