// @flow
"use strict";
import React from "react";
import Question from "./Question";

if (process.env.CLIENT) {
	require("../styles/questionsList.css");
}

import type {QuestionType} from "../flowTypes/questions.flow";
import type {VoteType} from "../../votes/";
type QuestionsListPropsType = {|
	data: Array<QuestionType>,
	getUserVote?: (number) => ?VoteType,
	title: string
|};

const QuestionsList = (props: QuestionsListPropsType) => {
	const generatedQuestions = props.data.map((question, index) => {
		const userVote = props.getUserVote ? props.getUserVote(question.id) : null;
		return (
			<Question
				key={index}
				data={question}
				userVote={userVote} />
		);
	});
	return (
		<div className="questions-list body-content-margin">
			<h3 className="askit-blue">{props.title}</h3>
			{generatedQuestions.length > 0
				? generatedQuestions
				: <p className="no-results">No questions here</p>}
		</div>
	);
};
export default QuestionsList;
