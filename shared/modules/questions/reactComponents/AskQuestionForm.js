// @flow
"use strict";
import React from "react";

type AskQuestionFormPropsType = {
	value: string,
	textChange: (SyntheticInputEvent) => void,
	submit: () => void
};

const AskQuestionForm = (props: AskQuestionFormPropsType) => {
	return (
		<div className="ask-question-form body-content-margin">
			<textarea
				rows={4}
				cols={100}
				placeholder="Ask your question..."
				onChange={props.textChange}
				value={props.value} />
			<button className="btn btn-default askit-blue-button" onClick={props.submit}>
				Submit
			</button>
		</div>
	);
};
export default AskQuestionForm;
