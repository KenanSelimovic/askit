// @flow
"use strict";
import React from "react";
import AskQuestionForm from "./AskQuestionForm";
import {selectors as authSelectors} from "../../auth/";
import {connect} from "react-redux";
import * as questionsActions from "../actions/actions";
import {baseUrl} from "../../../config/";
import logError from "../../../../client/modules/logging/errorLogger";
import toastr from "toastr";
import {validateText} from "../../../helpers/validations/validations";
import {config} from "../../../config/index";

if (process.env.CLIENT) {
	require("../styles/askQuestion.css");
}

import type {SubmitQuestionResponseType} from "../flowTypes/apiResponses.flow";
import type {GlobalAppStateType} from "../../../redux/defaultState";
import type {Dispatch} from "redux";
type AskQuestionPropsType = {
	token: string,
	refetchQuestions: () => void
};
type AskQuestionStateType = {
	textValue: string
};

class AskQuestion extends React.PureComponent {
	props: AskQuestionPropsType;
	state: AskQuestionStateType = {
		textValue: ""
	};
	submit = () => {
		const {
			text_min_len,
			text_max_len
		} = config.questions;
		const {
			textValue: text
		} = this.state;

		if (!validateText(text, text_min_len, text_max_len)) {
			toastr.remove();
			if (text && text.length > text_max_len) {
				toastr.warning(`Max question length is ${text_max_len} characters`);
			}
			else {
				toastr.warning("Please enter a valid question");
			}
			return;
		}
		const {token} = this.props;
		const fetchUrl = `${baseUrl}/v1/questions`;
		const submissionData = {
			text: this.state.textValue
		};
		fetch(fetchUrl, {
			method: "POST",
			headers: {
				authorization: token,
				"Content-type": "application/json"
			},
			body: JSON.stringify({data: submissionData})
		})
			.then(unparsed => unparsed.json())
			.then((response: SubmitQuestionResponseType) => {
				if (response.status !== 200 || !response.data) {
					logError(
						new Error(
							`Failed submitting question: ${JSON.stringify(response)}`
						)
					);
					toastr.error("Unknown error occurred");
					return;
				}
				this.setState(() => ({
					textValue: ""
				}));
				this.props.refetchQuestions();
			})
			.catch(err => {
				logError(err);
				toastr.error("Unknown error occurred");
			});
	};
	textChange = (e: SyntheticInputEvent) => {
		const {value} = e.target;
		this.setState(() => ({
			textValue: value
		}));
	};
	render() {
		return (
			<div className="ask-question">
				<AskQuestionForm
					submit={this.submit}
					textChange={this.textChange}
					value={this.state.textValue} />
			</div>
		);
	}
}
const mapStateToProps = (state: GlobalAppStateType) => ({
	token: authSelectors.getToken(state)
});
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	refetchQuestions: () => {
		dispatch(questionsActions.loadLatestQuestions(1));
	}
});
export default connect(mapStateToProps, mapDispatchToProps)(AskQuestion);
