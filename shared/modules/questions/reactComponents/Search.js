// @flow
"use strict";
import React from "react";
import QuestionsList from "./QuestionsList";
import * as questionsActions from "../actions/actions";
import {connect} from "react-redux";
import {
	getSearchQuestions,
	getSearchQuestionsTotalPages,
	getSearchText
} from "../selectors/questionSelectors";
import {
	selectors as votesSelectors
} from "../../votes/";
import type {QuestionType} from "../flowTypes/questions.flow";
import LoadMoreButton from "./LoadMoreButton";
import getUserVoteForQuestion from "../methods/getUserVoteForQuestion";
import {config} from "../../../config/index";
import SearchQuestionsForm from "./SearchQuestionsForm";
import toastr from "toastr";
import {validateText} from "../../../helpers/validations/validations";

if (process.env.CLIENT) {
	require("../styles/search.css");
}

const PER_PAGE = config.questions.questions_per_page;

import type {GlobalAppStateType} from "../../../redux/defaultState";
import type {Dispatch} from "redux";
import type {VoteDataType, VoteType} from "../../votes/";
type SearchStateType = {
	page: number,
	text: string
};
type SearchPropsType = {
	questions: Array<QuestionType>,
	totalPages: number,
	loadQuestions: (page: number, text?: string) => void,
	userVotes: Array<VoteDataType>,
	searchText: string
};

class Search extends React.PureComponent {
	props: SearchPropsType;
	state: SearchStateType = {
		page: 1,
		text: ""
	};
	componentDidMount = () => {
		this.setState(() => ({
			text: this.props.searchText
		}));
	};
	submit = (e: Event) => {
		e.preventDefault();
		const {
			page,
			text
		} = this.state;
		const {search_text_min_len, search_text_max_len} = config.questions;
		if (!validateText(text, search_text_min_len, search_text_max_len)) {
			if (text.length > search_text_max_len) {
				toastr.warning("Please enter a shorter query");
			}
			else {
				toastr.warning("Please enter a longer query");
			}
			return;
		}
		this.props.loadQuestions(page, text);
	};
	loadMore = () => {
		this.setState((prevState: SearchStateType) => ({
			page: prevState.page + 1
		}), this.fetchIfNeeded);
	};
	fetchIfNeeded = () => {
		const {page} = this.state;
		if (!this.pageIsLoaded(page)) {
			this.props.loadQuestions(page);
		}
		const nextPage = page + 1;
		if (nextPage <= this.props.totalPages) {
			if (!this.pageIsLoaded(nextPage)) {
				this.props.loadQuestions(nextPage);
			}
		}
	};
	getUserVoteForQuestion = (questionId: number): ?VoteType => {
		return getUserVoteForQuestion(questionId, this.props.userVotes);
	};
	pageIsLoaded = (page: number) => {
		const sliceStart = page * PER_PAGE - PER_PAGE;
		const sliceEnd = sliceStart + PER_PAGE;
		return this.props.questions.slice(sliceStart, sliceEnd).length !== 0;
	}
	getCurrentQuestions = (page: number = this.state.page) => {
		const sliceEnd = page * PER_PAGE;
		return this.props.questions.slice(0, sliceEnd);
	}
	textChange = (e: SyntheticInputEvent) => {
		const {value} = e.target;
		this.setState(() => ({
			text: value
		}));
	};
	render() {
		const {
			totalPages
		} = this.props;
		const {
			page,
			text
		} = this.state;
		const questionsData = this.getCurrentQuestions();
		return (
			<div className="search container ">
				<SearchQuestionsForm
					textChange={this.textChange}
					value={text}
					submit={this.submit} />
				<QuestionsList
					data={questionsData}
					getUserVote={this.getUserVoteForQuestion}
					title="Search results" />
				{questionsData.length > 0 && page !== totalPages &&
				<LoadMoreButton
					action={this.loadMore} />}
			</div>
		);
	}
}
const mapStateToProps = (state: GlobalAppStateType) => {
	return ({
		questions: getSearchQuestions(state),
		totalPages: getSearchQuestionsTotalPages(state),
		userVotes: votesSelectors.getUserVotes(state),
		searchText: getSearchText(state)
	});
};
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	loadQuestions: (page: number = 1, text: string = "") => {
		dispatch(questionsActions.searchQuestions(text, page));
	}
});
export default connect(mapStateToProps, mapDispatchToProps)(Search);
