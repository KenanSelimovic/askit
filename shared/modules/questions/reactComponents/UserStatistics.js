// @flow
"use strict";
import React from "react";
import getFullUrl from "../../../helpers/getFullUrl";

if (process.env.CLIENT) {
	require("../styles/userStatistics.css");
}

// flow types
import type {UserStatisticsType} from "../flowTypes/questions.flow";
type UserStatisticsPropsType = {|
	data: UserStatisticsType
|};

const UserStatistics = (props: UserStatisticsPropsType) => {
	const {
		full_name,
		num_answers,
		num_questions,
		num_likes
	} = props.data;
	const profileImageUrl = getFullUrl(props.data.profile_image_url);
	return (
		<div className="user-statistics container">
			<div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<img
					src={profileImageUrl}
					className="user-image" />
				<p className="full-name">
					{full_name}
				</p>
			</div>
			<div className="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<ul className="statistics-container">
					<li>{num_answers} answers given</li>
					<li>{num_questions} questions asked</li>
					<li>{num_likes} likes received</li>
				</ul>
			</div>
		</div>
	);
};
export default UserStatistics;
