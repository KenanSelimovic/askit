// @flow
"use strict";
import React from "react";
import QuestionsList from "./QuestionsList";
import LoadMoreButton from "./LoadMoreButton";
import type {QuestionType} from "../flowTypes/questions.flow";
import {connect} from "react-redux";
import {selectors as authSelectors} from "../../auth/";
import {requireAuth} from "../../auth/";
import * as actions from "../actions/actions";
import {config} from "../../../config/index";
import * as questionSelectors from "../selectors/questionSelectors";

const PER_PAGE = config.questions.questions_per_page;

import type {GlobalAppStateType} from "../../../redux/defaultState";
type MyQuestionsStateType = {|
	page: number
|};
type MyQuestionsPropsType = {|
	token: string,
	questions: Array<QuestionType>,
	totalPages: number,
	loadMyQuestions: (page: number) => void
|};

class MyQuestions extends React.PureComponent {
	props: MyQuestionsPropsType;
	state: MyQuestionsStateType = {
		page: 1
	};
	componentDidMount = () => {
		this.props.loadMyQuestions(1);
		this.props.loadMyQuestions(2);
	};
	loadMore = () => {
		this.setState((prevState: MyQuestionsStateType) => ({
			page: prevState.page + 1
		}), this.fetchIfNeeded);
	};
	fetchIfNeeded = () => {
		const {page} = this.state;
		if (!this.pageIsLoaded(page)) {
			this.props.loadMyQuestions(page);
		}
		const nextPage = page + 1;
		if (nextPage <= this.props.totalPages) {
			if (!this.pageIsLoaded(nextPage)) {
				this.props.loadMyQuestions(nextPage);
			}
		}
	};
	pageIsLoaded = (page: number) => {
		const sliceStart = page * PER_PAGE - PER_PAGE;
		const sliceEnd = sliceStart + PER_PAGE;
		return this.props.questions.slice(sliceStart, sliceEnd).length !== 0;
	}
	getCurrentQuestions = (page: number = this.state.page) => {
		const sliceEnd = page * PER_PAGE;
		return this.props.questions.slice(0, sliceEnd);
	}
	render() {
		const {
			totalPages
		} = this.props;
		const {
			page
		} = this.state;
		const questions = this.getCurrentQuestions();
		return (
			<div className="my-questions container ">
				<QuestionsList
					data={questions}
					title="My questions" />
				{!!totalPages && page !== totalPages &&
				<LoadMoreButton
					action={this.loadMore} />}
			</div>
		);
	}
}
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	loadMyQuestions: (page: number) => {
		dispatch(actions.loadMyQuestions(page));
	}
});
const mapStateToProps = (state: GlobalAppStateType) => ({
	token: authSelectors.getToken(state),
	questions: questionSelectors.getMyQuestions(state),
	totalPages: questionSelectors.getMyQuestionsTotalPages(state)
});
const ConnectedMyQuestions = connect(mapStateToProps, mapDispatchToProps)(MyQuestions);
export default requireAuth(ConnectedMyQuestions);
