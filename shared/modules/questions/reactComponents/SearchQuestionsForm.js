// @flow
"use strict";
import React from "react";

type SearchQuestionsFormPropsType = {
	value: string,
	textChange: (SyntheticInputEvent) => void,
	submit: (Event) => void
};

const SearchQuestionsForm = (props: SearchQuestionsFormPropsType) => {
	return (
		<div className="search-questions-form body-content-margin form-group">
			<form onSubmit={props.submit}>
				<input
					type="text"
					className="form-control"
					placeholder="Search for questions..."
					onChange={props.textChange}
					value={props.value} />
				<button
					className="btn btn-default askit-blue-button"
					onClick={props.submit}
					type="submit"
				>
					Search
				</button>
			</form>
		</div>
	);
};
export default SearchQuestionsForm;
