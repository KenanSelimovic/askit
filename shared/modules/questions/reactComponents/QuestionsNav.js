// @flow
"use strict";
import React from "react";

if (process.env.CLIENT) {
	require("../styles/questionNav.css");
}

type QuestionsNavPropsType = {|
	changeView: (number) => void
|};

const QuestionsNav = (props: QuestionsNavPropsType) => {
	return (
		<div className="question-nav">
			<span
				className="glyphicon glyphicon-question-sign askit-blue"
				title="Latest questions"
				onClick={props.changeView.bind(null, 1)} />
			<span
				className="glyphicon glyphicon-user askit-blue"
				title="Most active users"
				onClick={props.changeView.bind(null, 2)} />
			<span
				className="glyphicon glyphicon-fire askit-blue"
				title="Hot questions"
				onClick={props.changeView.bind(null, 3)} />
			<span
				className="glyphicon glyphicon-search askit-blue"
				title="Search questions"
				onClick={props.changeView.bind(null, 4)} />
		</div>
	);
};
export default QuestionsNav;
