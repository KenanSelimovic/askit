// @flow
"use strict";
import React from "react";
import HotQuestionsList from "./QuestionsList";
import {connect} from "react-redux";
import {
	selectors as votesSelectors
} from "../../votes/";
import type {QuestionType} from "../flowTypes/questions.flow";
import getUserVoteForQuestion from "../methods/getUserVoteForQuestion";
import * as actions from "../actions/actions";
import * as questionSelectors from "../selectors/questionSelectors";

import type {GlobalAppStateType} from "../../../redux/defaultState";
import type {VoteDataType, VoteType} from "../../votes/";
type HotQuestionsPropsType = {
	userVotes: Array<VoteDataType>,
	loadHotQuestions: () => void,
	questions: Array<QuestionType>
};

class HotQuestions extends React.PureComponent {
	props: HotQuestionsPropsType;
	componentDidMount = () => {
		this.fetchData();
	};
	fetchData = () => {
		this.props.loadHotQuestions();
	};
	getUserVoteForQuestion = (questionId: number): ?VoteType => {
		return getUserVoteForQuestion(questionId, this.props.userVotes);
	};
	render() {
		return (
			<div className="hot-questions container ">
				<HotQuestionsList
					data={this.props.questions}
					getUserVote={this.getUserVoteForQuestion}
					title="Hot questions" />
			</div>
		);
	}
}
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	loadHotQuestions: () => {
		dispatch(actions.loadHotQuestions());
	}
});
const mapStateToProps = (state: GlobalAppStateType) => ({
	userVotes: votesSelectors.getUserVotes(state),
	questions: questionSelectors.getHotQuestions(state)
});
export default connect(mapStateToProps, mapDispatchToProps)(HotQuestions);
