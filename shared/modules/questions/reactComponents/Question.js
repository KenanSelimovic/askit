// @flow
"use strict";
import React from "react";
import getFullUrl from "../../../helpers/getFullUrl";
import {config} from "../../../config/index";
import {baseUrl} from "../../../config/";
import {selectors as authSelectors} from "../../auth/";
import {actions as votesActions} from "../../votes/";
import {connect} from "react-redux";
import logError from "../../../../client/modules/logging/errorLogger";
import toastr from "toastr";
import VoteIcon from "../../common/reactComponents/VoteIcon";
import {Link} from "react-router";
import * as questionsActions from "../actions/actions";
import getTimePassed from "../../../helpers/getTimePassed";

if (process.env.CLIENT) {
	require("../styles/question.css");
}

// flow types
import type {Dispatch} from "redux";
import type {VoteType} from "../../votes/";
import type {GlobalAppStateType} from "../../../redux/defaultState";
import type {QuestionType} from "../flowTypes/questions.flow";
import type {QuestionVotingResponseType} from "../flowTypes/apiResponses.flow";
type QuestionPropsType = {|
	data: QuestionType,
	token: ?string,
	userId: ?number,
	userVote: ?VoteType,
	refetchUserVotes: (token: string) => void,
	refetchSingleQuestion: (number) => void
|};

class Question extends React.PureComponent {
	props: QuestionPropsType;
	voteUp = () => {
		this.submitVote("up");
	};
	voteDown = () => {
		this.submitVote("down");
	};
	submitVote = (type: VoteType) => {
		const {token} = this.props;
		if (!token) {
			toastr.remove();
			toastr.warning("Please log in to vote");
			return;
		}
		const {id, user} = this.props.data;
		if (this.props.userId === user.id) {
			return;
		}

		const submissionData = {
			vote_type: type
		};
		const submissionUrl = `${baseUrl}/v1/questions/${id}/vote`;
		fetch(submissionUrl, {
			method: "POST",
			headers: {
				authorization: token,
				"Content-type": "application/json"
			},
			body: JSON.stringify({data: submissionData})
		})
			.then(unparsed => unparsed.json())
			.then((votingResponse: QuestionVotingResponseType) => {
				if (votingResponse.status !== 200) {
					toastr.error("Unknown error occurred");
					logError(
						new Error(
							`Failed voting for answer: ${JSON.stringify(votingResponse)}`
						)
					);
					return;
				}
				this.props.refetchUserVotes(token);
				this.props.refetchSingleQuestion(id);
			})
			.catch(err => {
				logError(err);
				toastr.remove();
				toastr.error("Unknown error occurred");
			});
	};
	render() {
		const {
			data: {
				id,
				user,
				text,
				votes,
				answers,
				time
			},
			userVote
		} = this.props;
		const userProfileImage = getFullUrl(user.profile_image_url);
		const fullName = user.full_name || config.users.default_username;
		const {up: numVotesUp, down: numVotesDown} = votes;
		const ownQuestion = this.props.userId === user.id;
		const userAnswered = !!answers.find(answer => answer.user.id === this.props.userId);
		const timePassed = getTimePassed(time);
		return (
			<div className={`question ${ownQuestion ? "own-question" : ""}`}>
				<div className="user-info">
					<img src={userProfileImage} className="user-image" />
					<div className="name-and-time">
						<p className="full-name">{fullName || "User"}</p>
						<p className="time-passed">
							{timePassed}
						</p>
					</div>
				</div>
				<div className="question-text">
					<Link to={`/question/${id}`}>
						<p>{text}</p>
					</Link>
					<div className="vote-comment">
						<VoteIcon
							type="up"
							numVotes={numVotesUp}
							onClick={this.voteUp}
							active={userVote === "up"} />
						<VoteIcon
							type="down"
							numVotes={numVotesDown}
							onClick={this.voteDown}
							active={userVote === "down"} />
						<div className="comment-icon">
							<span className={`glyphicon glyphicon-comment ${userAnswered ? "active" : ""}`} />
							{this.props.data.answers.length}
						</div>
					</div>
				</div>
			</div>
		);
	}
}
const mapStateToProps = (state: GlobalAppStateType) => ({
	token: authSelectors.getToken(state),
	userId: authSelectors.getUserId(state)
});
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	refetchUserVotes: (token: string) => {
		dispatch(votesActions.loadUserVotes(token));
	},
	refetchSingleQuestion: (id: number) => {
		dispatch(questionsActions.loadSingleQuestion(id));
	}
});
export default connect(mapStateToProps, mapDispatchToProps)(Question);
