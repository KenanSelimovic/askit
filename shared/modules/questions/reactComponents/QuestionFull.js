// @flow
"use strict";
import React from "react";
import getFullUrl from "../../../helpers/getFullUrl";
import {config} from "../../../config/index";
import {baseUrl} from "../../../config/";
import {selectors as authSelectors} from "../../auth/";
import {getQuestion} from "../selectors/questionSelectors";
import {connect} from "react-redux";
import logError from "../../../../client/modules/logging/errorLogger";
import toastr from "toastr";
import VoteIcon from "../../common/reactComponents/VoteIcon";
import getUserVoteForQuestion from "../methods/getUserVoteForQuestion";
import {
	selectors as votesSelectors
} from "../../votes/";
import {EnterAnswer, AnswersList} from "../../answers";
import {actions as votesActions} from "../../votes/";
import * as questionsActions from "../actions/actions";
import fetchSingleQuestion from "../methods/fetchSingleQuestion";
import getTimePassed from "../../../helpers/getTimePassed";

if (process.env.CLIENT) {
	require("../styles/questionFull.css");
}

// flow types
import type {Dispatch} from "redux";
import type {VoteType, VoteDataType} from "../../votes/";
import type {GlobalAppStateType} from "../../../redux/defaultState";
import type {QuestionType} from "../flowTypes/questions.flow";
import type {
	QuestionVotingResponseType
} from "../flowTypes/apiResponses.flow";
type QuestionFullPropsType = {|
	data: ?QuestionType,
	token: ?string,
	userId: ?number,
	userVote: ?VoteType,
	params: {
		id: string
	},
	userVotes: Array<VoteDataType>,
	router: {
		push: (string) => void
	},
	refetchUserVotes: (token: string) => void,
	refetchSingleQuestion: (number) => void
|};
type QuestionFullStateType = {|
	data: ?QuestionType
|};

class QuestionFull extends React.PureComponent {
	props: QuestionFullPropsType;
	state: QuestionFullStateType = {
		data: null
	};
	componentDidMount = () => {
		const {data} = this.props;
		if (data) {
			this.setState(() => ({
				data
			}));
		}
		else {
			this.fetchData();
		}
	}
	fetchData = () => {
		const id = parseInt(this.props.params.id, 10);
		fetchSingleQuestion(id)
			.then(question => {
				if (!question) {
					this.props.router.push("/");
					return;
				}
				this.setState(() => ({
					data: question
				}));
			})
			.catch(err => {
				logError(err);
				toastr.error("Unknown error occurred");
			});
	};
	voteUp = () => {
		this.submitVote("up");
	};
	voteDown = () => {
		this.submitVote("down");
	};
	submitVote = (type: VoteType) => {
		const {token} = this.props;
		const {data} = this.state;
		if (!token || !data) {
			return;
		}
		if (this.props.userId === data.user.id) {
			return;
		}
		const {id} = data;
		const submissionData = {
			vote_type: type
		};
		const submissionUrl = `${baseUrl}/v1/questions/${id}/vote`;
		fetch(submissionUrl, {
			method: "POST",
			headers: {
				authorization: token,
				"Content-type": "application/json"
			},
			body: JSON.stringify({data: submissionData})
		})
			.then(unparsed => unparsed.json())
			.then((votingResponse: QuestionVotingResponseType) => {
				if (votingResponse.status !== 200) {
					logError(
						new Error(
							`Failed retrieving question id ${id}: ${JSON.stringify(votingResponse)}`
						)
					);
					toastr.error("Unknown error occurred");
					return;
				}
				this.fetchData();
				this.props.refetchUserVotes(token);
				this.props.refetchSingleQuestion(id);
			})
			.catch(err => {
				logError(err);
				toastr.remove();
				toastr.error("Unknown error occurred");
			});
	};
	refetch = () => {
		const {data} = this.props;
		if (!data) {
			return;
		}
		this.fetchData();
		this.props.refetchSingleQuestion(data.id);
	}
	render() {
		const {data} = this.state;
		if (!data) {
			return null;
		}
		const {
			user,
			text,
			votes
		} = data;
		const userVote = getUserVoteForQuestion(data.id, this.props.userVotes);
		const userProfileImage = getFullUrl(user.profile_image_url);
		const fullName = user.full_name || config.users.default_username;
		const {up: numVotesUp, down: numVotesDown} = votes;
		const ownQuestion = this.props.userId === user.id;
		const timePassed = getTimePassed(data.time);
		return (
			<div className={`question-full container ${ownQuestion ? "own-question" : ""}`}>
				<div className="user-info container">
					<img src={userProfileImage} className="user-image" />
					<div className="name-and-time">
						<p className="full-name">{fullName || "User"}</p>
						<p>{timePassed}</p>
					</div>
				</div>
				<div className="question-main container">
					<div className="row">
						<div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
							<p className="question-text">
								{text}
							</p>
						</div>
						<div className="col-lg-9 col-md-9 col-sm-9 col-xs-12">
							<div className="question-vote-icons">
								<VoteIcon
									type="up"
									numVotes={numVotesUp}
									onClick={this.voteUp}
									active={userVote === "up"} />
								<VoteIcon
									type="down"
									numVotes={numVotesDown}
									onClick={this.voteDown}
									active={userVote === "down"} />
							</div>
						</div>
					</div>
				</div>
				{this.props.token &&
				<EnterAnswer
					questionData={data}
					triggerRefetch={this.fetchData} />}
				<AnswersList
					data={data.answers}
					refetch={this.refetch} />
			</div>
		);
	}
}
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	refetchUserVotes: (token: string) => {
		dispatch(votesActions.loadUserVotes(token));
	},
	refetchSingleQuestion: (id: number) => {
		dispatch(questionsActions.loadSingleQuestion(id));
	}
});
const mapStateToProps = (state: GlobalAppStateType, props: QuestionFullPropsType) => ({
	token: authSelectors.getToken(state),
	userId: authSelectors.getUserId(state),
	data: getQuestion(parseInt(props.params.id, 10))(state),
	userVotes: votesSelectors.getUserVotes(state)
});
export default connect(mapStateToProps, mapDispatchToProps)(QuestionFull);
