// @flow
"use strict";
import React from "react";
import TopUsersList from "./TopUsersList";
import * as questionsActions from "../actions/actions";
import {getTopUsers} from "../selectors/questionSelectors";
import {connect} from "react-redux";

import type {GlobalAppStateType} from "../../../redux/defaultState";
import type {Dispatch} from "redux";
import type {UserStatisticsType} from "../flowTypes/questions.flow";
type TopUsersPropsType = {
	loadTopUsers: () => void,
	users: Array<UserStatisticsType>
};

class TopUsers extends React.PureComponent {
	props: TopUsersPropsType;
	componentDidMount = () => {
		this.fetchData();
	};
	fetchData = () => {
		this.props.loadTopUsers();
	};
	render() {
		return (
			<div className="top-users container ">
				<TopUsersList
					data={this.props.users} />
			</div>
		);
	}
}
const mapStateToProps = (state: GlobalAppStateType) => ({
	users: getTopUsers(state)
});
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	loadTopUsers: () => {
		dispatch(questionsActions.loadTopUsers());
	}
});
export default connect(mapStateToProps, mapDispatchToProps)(TopUsers);
