// @flow
"use strict";
import type {GlobalAppStateType} from "../../../redux/defaultState";


export const getQuestion = (id: number) => (state: GlobalAppStateType) => {
	return state.questions.latestQuestions.data.find(question => question.id === id);
};
export const getQuestions = (state: GlobalAppStateType) => {
	return state.questions.latestQuestions.data;
};
export const getHotQuestions = (state: GlobalAppStateType) => {
	return state.questions.hotQuestions.data;
};
export const getMyQuestions = (state: GlobalAppStateType) => {
	return state.questions.myQuestions.data;
};
export const getMyQuestionsTotalPages = (state: GlobalAppStateType) => {
	return state.questions.myQuestions.totalPages;
};
export const getTotalPages = (state: GlobalAppStateType) => {
	return state.questions.latestQuestions.totalPages;
};
export const getTopUsers = (state: GlobalAppStateType) => {
	return state.topUsers.data;
};
export const getSearchQuestions = (state: GlobalAppStateType) => {
	return state.questions.search.data;
};
export const getSearchQuestionsTotalPages = (state: GlobalAppStateType) => {
	return state.questions.search.totalPages;
};
export const getSearchText = (state: GlobalAppStateType) => {
	return state.questions.search.searchText;
};
