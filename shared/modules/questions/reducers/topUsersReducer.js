// @flow
"use strict";
import * as actionTypes from "../actions/types";
import defaultState from "../../../redux/defaultState";

import type {QuestionsActionType} from "../actions/actions.flow";
import type {UserStatisticsType} from "../flowTypes/questions.flow";

export type TopUsersStateType = {|
	data: Array<UserStatisticsType>
|};
// eslint-disable-next-line max-len
export default (state: TopUsersStateType = defaultState.topUsers, action: QuestionsActionType): TopUsersStateType => {
	switch (action.type) {
	case actionTypes.TOP_USERS_LOAD_SUCCESS: {
		return Object.assign(
			{},
			state,
			{
				data: action.data.users
			}
		);
	}
	default:
		return state;
	}
};
