// @flow
"use strict";
import {combineReducers} from "redux";
import latestQuestions from "./latestQuestionsReducer";
import hotQuestions from "./hotQuestionsReducer";
import myQuestions from "./myQuestionsReducer";
import search from "./searchReducer";

import type {LatestQuestionsStateType} from "./latestQuestionsReducer";
import type {HotQuestionsStateType} from "./hotQuestionsReducer";
import type {MyQuestionsStateType} from "./myQuestionsReducer";
import type {SearchQuestionsStateType} from "./searchReducer";
export type QuestionsStateType = {|
	hotQuestions: HotQuestionsStateType,
	latestQuestions: LatestQuestionsStateType,
	myQuestions: MyQuestionsStateType,
	search: SearchQuestionsStateType
|};
export default combineReducers({
	hotQuestions,
	latestQuestions,
	myQuestions,
	search
});
