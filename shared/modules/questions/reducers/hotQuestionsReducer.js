// @flow
"use strict";
import * as actionTypes from "../actions/types";
import defaultState from "../../../redux/defaultState";

import type {QuestionsActionType} from "../actions/actions.flow";
import type {QuestionType} from "../flowTypes/questions.flow";

export type HotQuestionsStateType = {|
	data: Array<QuestionType>
|};
// eslint-disable-next-line max-len
export default (state: HotQuestionsStateType = defaultState.questions.hotQuestions, action: QuestionsActionType): HotQuestionsStateType => {
	switch (action.type) {
	case actionTypes.HOT_QUESTIONS_LOAD_SUCCESS: {
		return Object.assign(
			{},
			state,
			{
				data: action.data.questions
			}
		);
	}
	case actionTypes.QUESTION_LOAD_SUCCESS: {
		const {id} = action.data.question;
		const {data: hotQuestions} = state;
		const hotQuestionIndex
			= hotQuestions
				.findIndex(question => question.id === id);

		if (hotQuestionIndex === -1) {
			return state;
		}
		const newHotQuestions = [
			...hotQuestions.slice(0, hotQuestionIndex),
			action.data.question,
			...hotQuestions.slice(hotQuestionIndex + 1)
		];
		return Object.assign(
			{},
			state,
			{
				data: newHotQuestions
			}
		);
	}
	default:
		return state;
	}
};
