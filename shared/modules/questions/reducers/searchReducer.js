// @flow
"use strict";
import * as actionTypes from "../actions/types";
import defaultState from "../../../redux/defaultState";
import questionExistsInArray from "../methods/questionExistsInArray";

import type {QuestionsActionType} from "../actions/actions.flow";
import type {QuestionType} from "../flowTypes/questions.flow";

export type SearchQuestionsStateType = {|
	data: Array<QuestionType>,
	totalPages: number,
	searchText: string
|};
// eslint-disable-next-line max-len
export default (state: SearchQuestionsStateType = defaultState.questions.search, action: QuestionsActionType): SearchQuestionsStateType => {
	switch (action.type) {
	case actionTypes.SEARCH_QUESTIONS_LOAD_SUCCESS: {
		const {page, perPage, totalPages, questions: questionsInAction} = action.data;
		const {data: questionsInState} = state;
		const receivedQuestionsStart = (page - 1) * perPage;
		const receivedQuestionsEnd = page * perPage;

		// some of the questions received in action can be the
		// same ones as existing in state, so we fillter
		// questions in state
		const filteredStateQuestionsFirstPart
			=	questionsInState
				.slice(0, receivedQuestionsStart)
				.filter(question => !questionExistsInArray(questionsInAction, question.id));
		const filteredStateQuestionsSecondPart
			=	questionsInState
				.slice(receivedQuestionsEnd)
				.filter(question => !questionExistsInArray(questionsInAction, question.id));

		const newQuestions = [
			...filteredStateQuestionsFirstPart,
			...questionsInAction,
			...filteredStateQuestionsSecondPart
		];
		return Object.assign(
			{},
			state,
			{
				data: newQuestions,
				totalPages,
				searchText: action.data.searchText
			}
		);
	}
	case actionTypes.QUESTION_LOAD_SUCCESS: {
		const {id} = action.data.question;
		const {data: searchQuestions} = state;
		const searchQuestionIndex
			= searchQuestions
				.findIndex(question => question.id === id);

		if (searchQuestionIndex === -1) {
			return state;
		}
		const newSearchQuestions = [
			...searchQuestions.slice(0, searchQuestionIndex),
			action.data.question,
			...searchQuestions.slice(searchQuestionIndex + 1)
		];
		return Object.assign(
			{},
			state,
			{
				data: newSearchQuestions
			}
		);
	}
	default:
		return state;
	}
};
