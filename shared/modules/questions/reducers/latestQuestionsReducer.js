// @flow
"use strict";
import * as actionTypes from "../actions/types";
import defaultState from "../../../redux/defaultState";
import questionExistsInArray from "../methods/questionExistsInArray";

import type {QuestionsActionType} from "../actions/actions.flow";
import type {QuestionType} from "../flowTypes/questions.flow";

export type LatestQuestionsStateType = {|
	data: Array<QuestionType>,
	totalPages: number
|};
// eslint-disable-next-line max-len
export default (state: LatestQuestionsStateType = defaultState.questions.latestQuestions, action: QuestionsActionType): LatestQuestionsStateType => {
	switch (action.type) {
	case actionTypes.QUESTIONS_LOAD_SUCCESS: {
		const {page, perPage, totalPages, questions: questionsInAction} = action.data;
		const {data: questionsInState} = state;
		const receivedQuestionsStart = (page - 1) * perPage;
		const receivedQuestionsEnd = page * perPage;

		// some of the questions received in action can be the
		// same ones as existing in state, so we fillter
		// questions in state
		const filteredStateQuestionsFirstPart
			=	questionsInState
				.slice(0, receivedQuestionsStart)
				.filter(question => !questionExistsInArray(questionsInAction, question.id));
		const filteredStateQuestionsSecondPart
			=	questionsInState
				.slice(receivedQuestionsEnd)
				.filter(question => !questionExistsInArray(questionsInAction, question.id));

		const newQuestions = [
			...filteredStateQuestionsFirstPart,
			...questionsInAction,
			...filteredStateQuestionsSecondPart
		];
		return Object.assign(
			{},
			state,
			{
				data: newQuestions,
				totalPages
			}
		);
	}
	case actionTypes.QUESTION_LOAD_SUCCESS: {
		const {id} = action.data.question;
		const {data: latestQuestions} = state;
		const latestQuestionIndex
			= latestQuestions
				.findIndex(question => question.id === id);

		const newLatestQuestions = [
			...latestQuestions.slice(0, latestQuestionIndex),
			action.data.question,
			...latestQuestions.slice(latestQuestionIndex + 1)
		];
		return Object.assign(
			{},
			state,
			{
				data: newLatestQuestions
			}
		);
	}
	default:
		return state;
	}
};
