// @flow
"use strict";
import React from "react";
import Layout from "./layout/reactComponents/Layout";

type AppPropsType = {
	children: React.Element<mixed>
};

const App = ({children}: AppPropsType): React.Element<*> => {
	return (
		<Layout>
			{children}
		</Layout>
	);
};
export default App;
