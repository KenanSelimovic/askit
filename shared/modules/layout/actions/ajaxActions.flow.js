// @flow
"use strict";

export type BeginAjaxCallActionType = {|
	type: "BEGIN_AJAX_CALL",
	requestType: ?string
|};
export type AjaxCallErrorType = {|
	type: "AJAX_CALL_ERROR"
|};
export type AjaxCallSuccessType = {|
	type: "AJAX_SUCCESS"
|};

export type AjaxStatusActionType =
	BeginAjaxCallActionType
	| AjaxCallErrorType
	| AjaxCallSuccessType;
