// @flow
"use strict";
import * as types from "./types";

export const beginAjaxCall = (requestType: string) => {
	// used for tracking the number of ajax calls in progres
	// which is used for showing the loader
	// the requestType attribute is ONLY there to make tracking
	// what is going on in the client easier for developer
	// for example, in redux devtools, for each BEGIN_AJAX_CALL
	// action you can determine what's it actually doing
	// by checking this requestType attribute
	return ({
		type: types.BEGIN_AJAX_CALL,
		requestType: requestType || "not_specified"
	});
};

export const ajaxCallError = () => {
	return {
		type: types.AJAX_CALL_ERROR
	};
};
export const ajaxSuccess = () => {
	return {
		type: types.AJAX_SUCCESS
	};
};
