// @flow
"use strict";

import * as types from "../actions/types";
import type {AjaxStatusActionType} from "../actions/actions.flow";

const actionTypeEndsInSuccess = (type: string) => {
	return type.substring(type.length - 8) === "_SUCCESS";
};

const ajaxStatusReducer = (state: number = 0, action: AjaxStatusActionType) => {
	if (action.type === types.BEGIN_AJAX_CALL) {
		return state + 1;
	}
	else if (action.type === types.AJAX_CALL_ERROR || actionTypeEndsInSuccess(action.type)) {
		const decrementedNumberOfAjaxCalls = state - 1;
		return (decrementedNumberOfAjaxCalls < 0 ? 0 : decrementedNumberOfAjaxCalls);
	}
	return state;
};
export default ajaxStatusReducer;
