// @flow
"use strict";
import React from "react";
import {Link} from "react-router";

if (process.env.CLIENT) {
	require("../styles/logo.css");
}

const Logo = () => {
	return (
		<div className="logo">
			<h1 className="askit-blue">
				<Link to="/">Ask.it</Link>
			</h1>
		</div>
	);
};
export default Logo;
