// @flow
"use strict";
import React from "react";
import {Loader} from "react-loaders";
import {connect} from "react-redux";
import Navbar from "../../navbar/reactComponents/Navbar";
import Logo from "./Logo";

import type {GlobalAppStateType} from "../../../redux/defaultState";

if (process.env.CLIENT) {
	require("../styles/layout.css");
	require("../styles/forms.css");
}

type LayoutPropsType = {
	numCallsInProgress: number,
	children: Array<React.Element<*>>
};

const Layout = (props: LayoutPropsType) => {
	return (
		<div className="layout">
			<Navbar />
			<Logo />
			<Loader type="ball-scale-ripple-multiple" active={props.numCallsInProgress > 0}/>
			{props.children}
		</div>
	);
};

const mapStateToProps = (state: GlobalAppStateType) => ({
	numCallsInProgress: state.ajaxCallsInProgress
});
export default connect(mapStateToProps)(Layout);
