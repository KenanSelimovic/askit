// @flow
"use strict";

export type AnswerCreationResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		id: number
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};

export type AnswerUpdateResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		id: number
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};
export type AnswerDeletionResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		id: number
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};
export type AnswerVotingResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: null
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};
