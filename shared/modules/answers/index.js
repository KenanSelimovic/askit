// @flow
"use strict";

import EnterAnswerComponent from "./reactComponents/EnterAnswer";
import AnswersListComponent from "./reactComponents/AnswersList";

export type {AnswerType} from "./flowTypes/answers.flow";

export const EnterAnswer = EnterAnswerComponent;
export const AnswersList = AnswersListComponent;
