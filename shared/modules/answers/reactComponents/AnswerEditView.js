// @flow
"use strict";
import React from "react";

type AnswerEditViewPropsType = {|
	text: string,
	toggleEdit: () => void,
	submit: () => void,
	onChange: (SyntheticInputEvent) => void
|};

const AnswerEditView = (props: AnswerEditViewPropsType) => {
	const {
		text,
		onChange,
		toggleEdit,
		submit
	} = props;
	return (
		<div className="answer answer-edit-view own-answer">
			<textarea
				value={text}
				onChange={onChange}
				autoFocus
				rows={4}
				className="edit-answer-input" />
			<div className="answer-icons">
				<span
					title="Save"
					className="glyphicon glyphicon-ok"
					onClick={submit} />
				<span
					title="Cancel"
					className="glyphicon glyphicon-remove"
					onClick={toggleEdit} />
			</div>
		</div>
	);
};
export default AnswerEditView;
