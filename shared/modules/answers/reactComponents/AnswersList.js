// @flow
"use strict";
import React from "react";
import Answer from "./Answer";
import {selectors as authSelectors} from "../../auth/";
import {connect} from "react-redux";

import type {GlobalAppStateType} from "../../../redux/defaultState";
import type {AnswerType} from "../flowTypes/answers.flow";
type AnswersListPropsType = {
	data: Array<AnswerType>,
	userId: ?number,
	refetch: () => void
};

const AnswersList = (props: AnswersListPropsType) => {
	const generatedAnswers = props.data.map((answer, index) => {
		return (
			<div
				key={index}
				className="col-lg-7 col-lg-offset-2 col-md-8 col-md-offset-1 col-sm-12 col-xs-12"
			>
				<Answer
					data={answer}
					byCurrentUser={answer.user.id === props.userId}
					triggerRefetch={props.refetch} />
			</div>
		);
	});
	return (
		<div className="answers-list container">
			<div className="row">
				{generatedAnswers}
			</div>
		</div>
	);
};
const mapStateToProps = (state: GlobalAppStateType) => ({
	userId: authSelectors.getUserId(state)
});
export default connect(mapStateToProps)(AnswersList);
