// @flow
"use strict";
import React from "react";
import EnterAnswerView from "./EnterAnswerView";
import {baseUrl} from "../../../config/";
import {selectors as authSelectors} from "../../auth/";
import {connect} from "react-redux";
import {validateText} from "../../../helpers/validations/validations";
import {config} from "../../../config/index";
import toastr from "toastr";
import logError from "../../../../client/modules/logging/errorLogger";


import type {AnswerCreationResponseType} from "../flowTypes/apiResponses.flow";
import type {GlobalAppStateType} from "../../../redux/defaultState";
import type {QuestionType} from "../../questions/index.flow";
type EnterAnswerPropsType = {|
	questionData: QuestionType,
	token: string,
	triggerRefetch: () => void
|};
type EnterAnswerStateType = {
	text: string
};

class EnterAnswer extends React.PureComponent {
	props: EnterAnswerPropsType;
	state: EnterAnswerStateType = {
		text: ""
	};
	textChange = (e: SyntheticInputEvent) => {
		const {value} = e.target;
		this.setState(() => ({
			text: value
		}));
	};
	submit = () => {
		const {
			text_min_len,
			text_max_len
		} = config.answers;
		const {
			text
		} = this.state;
		if (!validateText(text, text_min_len, text_max_len)) {
			toastr.remove();
			if (text && text.length > text_max_len) {
				toastr.warning(`Max length is ${text_max_len} characters`);
			}
			else {
				toastr.warning("Please enter a valid answer");
			}
			return;
		}
		const questionId = this.props.questionData.id;
		const submissionUrl = `${baseUrl}/v1/answers/${questionId}`;
		const submissionData = {
			text: this.state.text
		};
		fetch(submissionUrl, {
			method: "POST",
			headers: {
				authorization: this.props.token,
				"Content-type": "application/json"
			},
			body: JSON.stringify({data: submissionData})
		})
			.then(unparsed => unparsed.json())
			.then((submissionResponse: AnswerCreationResponseType) => {
				if (submissionResponse.status !== 200) {
					toastr.error("Uknown error occurred");
					logError(
						new Error(
							`Failed submitting answer: ${JSON.stringify(submissionResponse)}`
						)
					);
					return;
				}
				this.setState(() => ({
					text: ""
				}));
				this.props.triggerRefetch();
			})
			.catch(err => {
				logError(err);
				toastr.error("Unknown error occurred");
			});
	};
	render() {
		return (
			<div className="enter-answer container">
				<div className="row">
					<EnterAnswerView
						value={this.state.text}
						textChange={this.textChange}
						submit={this.submit} />
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state: GlobalAppStateType) => ({
	token: authSelectors.getToken(state)
});
export default connect(mapStateToProps)(EnterAnswer);
