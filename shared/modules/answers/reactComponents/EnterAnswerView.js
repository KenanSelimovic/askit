// @flow
"use strict";
import React from "react";

type EnterAnswerViewPropsType = {
	textChange: (SyntheticInputEvent) => void,
	value: string,
	submit: () => void
};

const EnterAnswerView = (props: EnterAnswerViewPropsType) => {
	return (
		<div className="enter-answer-view col-lg-9 col-md-9 col-sm-9 col-xs-12">
			<textarea
				rows={4}
				cols={100}
				placeholder="Give an answer..."
				onChange={props.textChange}
				value={props.value} />
			<button
				className="btn btn-default askit-blue-button"
				onClick={props.submit}
			>
				Submit
			</button>
		</div>
	);
};
export default EnterAnswerView;
