// @flow
"use strict";
import React from "react";
import AnswerView from "./AnswerView";
import AnswerEditView from "./AnswerEditView";
import {config, baseUrl} from "../../../config/";
import {validateText} from "../../../helpers/validations/validations";
import toastr from "toastr";
import {selectors as authSelectors} from "../../auth/";
import {connect} from "react-redux";
import logError from "../../../../client/modules/logging/errorLogger";
import {actions as votesActions} from "../../votes/";
import {
	selectors as votesSelectors
} from "../../votes/";
import getUserVoteForAnswer from "../methods/getUserVoteForAnswer";

if (process.env.CLIENT) {
	require("../styles/answer.css");
	require("../styles/confirmDeletionDialog.css");
}

import type {VoteDataType, VoteType} from "../../votes/";
import type {Dispatch} from "redux";
import type {GlobalAppStateType} from "../../../redux/defaultState";
import type {AnswerType} from "../flowTypes/answers.flow";
import type {
	AnswerDeletionResponseType,
	AnswerVotingResponseType
} from "../flowTypes/apiResponses.flow";
type AnswerPropsType = {|
	data: AnswerType,
	byCurrentUser: boolean,
	token: ?string,
	triggerRefetch: () => void,
	refetchUserVotes: (token: string) => void,
	userVotes: Array<VoteDataType>
|};
type AnswerStateType = {|
	editMode: boolean,
	editTextValue: string
|};

class Answer extends React.PureComponent {
	props: AnswerPropsType;
	state: AnswerStateType = {
		editMode: false,
		editTextValue: ""
	};
	toggleEditMode = () => {
		this.setState((prevState: AnswerStateType, prevProps: AnswerPropsType) => ({
			editMode: !prevState.editMode,
			editTextValue: prevProps.data.text
		}));
	};
	editTextChange = (e: SyntheticInputEvent) => {
		const {value} = e.target;
		this.setState(() => ({
			editTextValue: value
		}));
	};
	submitEdit = () => {
		const {token} = this.props;
		if (!token) {
			return;
		}
		const {editTextValue: text} = this.state;
		const {
			text_min_len,
			text_max_len
		} = config.answers;
		if (!validateText(text, text_min_len, text_max_len)) {
			toastr.remove();
			toastr.warning("Please enter valid answer text");
			return;
		}
		const answerId = this.props.data.id;
		const submissionUrl = `${baseUrl}/v1/answers/${answerId}/edit`;
		const submissionData = {
			text
		};
		fetch(submissionUrl, {
			method: "POST",
			headers: {
				authorization: token,
				"Content-type": "application/json"
			},
			body: JSON.stringify({data: submissionData})
		})
			.then(unparsed => unparsed.json())
			.then((answerDeletionResponse: AnswerDeletionResponseType) => {
				if (answerDeletionResponse.status !== 200) {
					toastr.error("Unknown error occurred");
					logError(
						new Error(
							`Failed updating answer id ${answerId}:
							${JSON.stringify(answerDeletionResponse)}`
						)
					);
				}
				this.toggleEditMode();
				this.props.triggerRefetch();
			})
			.catch(err => {
				logError(err);
				toastr.error("Unknown error occurred");
			});
	};
	promptDeletionConfirmation = () => {
		return new Promise((resolve) => {
			window.bootbox.confirm({
				message: "Delete this answer?",
				callback: (result) => {
					resolve(result);
				},
				className: "confirm-deletion-dialog",
				buttons: {
					confirm: {
						label: 'Ok',
						className: 'askit-blue-button'
					},
					cancel: {
						label: 'Cancel',
						className: 'btn-default'
					}
				}
			});
		});
	};
	openDelete = () => {
		const {token} = this.props;
		if (!token) {
			return;
		}
		this.promptDeletionConfirmation()
			.then((answer: boolean) => {
				if (!answer) {
					return;
				}
				this.submitDeletion();
			});
	};
	submitDeletion = () => {
		const {token} = this.props;
		if (!token) {
			return;
		}
		const answerId = this.props.data.id;
		const submissionUrl = `${baseUrl}/v1/answers/${answerId}`;
		fetch(submissionUrl, {
			method: "DELETE",
			headers: {
				authorization: token,
				"Content-type": "application/json"
			}
		})
			.then(unparsed => unparsed.json())
			.then((answerDeletionResponse: AnswerDeletionResponseType) => {
				if (answerDeletionResponse.status !== 200) {
					toastr.error("Unknown error occurred");
					logError(
						new Error(
							`Failed deleting answer id ${answerId}:
							${JSON.stringify(answerDeletionResponse)}`
						)
					);
				}
				this.props.triggerRefetch();
			})
			.catch(err => {
				logError(err);
				toastr.error("Unknown error occurred");
			});
	}
	voteUp = () => {
		this.submitVote("up");
	};
	voteDown = () => {
		this.submitVote("down");
	};
	submitVote = (type: VoteType) => {
		const {token} = this.props;
		if (!token) {
			toastr.remove();
			toastr.warning("Please log in to vote");
			return;
		}
		if (this.props.byCurrentUser) {
			return;
		}
		const {id} = this.props.data;
		const submissionData = {
			vote_type: type
		};
		const submissionUrl = `${baseUrl}/v1/answers/${id}/vote`;
		fetch(submissionUrl, {
			method: "POST",
			headers: {
				authorization: token,
				"Content-type": "application/json"
			},
			body: JSON.stringify({data: submissionData})
		})
			.then(unparsed => unparsed.json())
			.then((votingResponse: AnswerVotingResponseType) => {
				if (votingResponse.status !== 200) {
					toastr.error("Unknown error occurred");
					logError(
						new Error(
							`Failed voting for answer: ${JSON.stringify(votingResponse)}`
						)
					);
					return;
				}
				this.props.triggerRefetch();
				this.props.refetchUserVotes(token);
			})
			.catch(err => {
				logError(err);
				toastr.remove();
				toastr.error("Unknown error occurred");
			});
	};
	getUserVoteForAnswer = (answerId: number): ?VoteType => {
		return getUserVoteForAnswer(
			answerId,
			this.props.userVotes
		);
	};
	render() {
		const {
			data,
			byCurrentUser
		} = this.props;
		const {
			editMode,
			editTextValue
		} = this.state;

		const userVote = this.getUserVoteForAnswer(data.id);

		if (!editMode) {
			return (
				<AnswerView
					byCurrentUser={byCurrentUser}
					data={data}
					toggleEdit={this.toggleEditMode}
					deleteAnswer={this.openDelete}
					voteUp={this.voteUp}
					voteDown={this.voteDown}
					userVote={userVote} />
			);
		}
		return (
			<AnswerEditView
				text={editTextValue}
				toggleEdit={this.toggleEditMode}
				submit={this.submitEdit}
				onChange={this.editTextChange} />
		);
	}
}
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	refetchUserVotes: (token: string) => {
		dispatch(votesActions.loadUserVotes(token));
	}
});
const mapStateToProps = (state: GlobalAppStateType) => ({
	userVotes: votesSelectors.getUserVotes(state),
	token: authSelectors.getToken(state)
});
export default connect(mapStateToProps, mapDispatchToProps)(Answer);
