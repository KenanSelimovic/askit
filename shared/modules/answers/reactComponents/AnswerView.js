// @flow
"use strict";
import React from "react";
import VoteIcon from "../../common/reactComponents/VoteIcon";
import getFullUrl from "../../../helpers/getFullUrl";
import getTimePassed from "../../../helpers/getTimePassed";

import type {VoteType} from "../../votes/";
import type {AnswerType} from "../flowTypes/answers.flow";
type AnswerViewPropsType = {|
	byCurrentUser: boolean,
	data: AnswerType,
	toggleEdit: () => void,
	deleteAnswer: () => void,
	voteUp: () => void,
	voteDown: () => void,
	userVote: ?VoteType
|};

const AnswerView = (props: AnswerViewPropsType) => {
	const {
		data,
		toggleEdit,
		deleteAnswer,
		byCurrentUser,
		userVote
	} = props;

	const profileImageUrl = getFullUrl(data.user.profile_image_url);
	const timePassedString = getTimePassed(data.time);

	return (
		<div className={`answer answer-view ${byCurrentUser ? "own-answer" : ""}`}>
			<div className="answer-author">
				<img
					className="user-image"
					src={profileImageUrl} />
				<div className="name-and-hours">
					<p className="full-name">
						{data.user.full_name}
					</p>
					<p className="hours-passed">
						{timePassedString}
					</p>
				</div>
			</div>
			<p className="answer-text">
				{data.text}
			</p>
			{byCurrentUser &&
			<div className="answer-icons">
				<span
					className="glyphicon glyphicon-edit"
					onClick={toggleEdit} />
				<span
					className="glyphicon glyphicon-trash"
					onClick={deleteAnswer} />
			</div>}
			<div className="vote-icons">
				<VoteIcon
					type="up"
					numVotes={data.votes.up}
					onClick={props.voteUp}
					active={userVote === "up"} />
				<VoteIcon
					type="down"
					numVotes={data.votes.down}
					onClick={props.voteDown}
					active={userVote === "down"} />
			</div>
		</div>
	);
};
export default AnswerView;
