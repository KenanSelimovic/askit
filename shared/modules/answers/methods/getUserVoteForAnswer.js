// @flow
"use strict";
import type {VoteDataType, VoteType} from "../../votes/";

export default (answerId: number, votes: Array<VoteDataType>): ?VoteType => {
	const userVote
		= votes
			.find(vote => vote.post_id === answerId);
	return userVote ? userVote.type : null;
};
