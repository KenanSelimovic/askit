// @flow
"use strict";
import {baseUrl} from "../../../config/";

import type {ProfileDataType} from "../flowTypes/profile.flow";
import type {GetProfileResponseType} from "../flowTypes/apiResponses.flow";

// eslint-disable-next-line immutable/no-mutation
module.exports = (token: string): Promise<?ProfileDataType> => {
	return new Promise(resolve => {
		if (!token) {
			resolve(null);
			return;
		}
		fetch(`${baseUrl}/v1/users/profile`, {
			headers: {
				authorization: token
			}
		})
			.then(unparsed => unparsed.json())
			.then((profileData: GetProfileResponseType) => {
				if (profileData.status !== 200 || profileData.data === null) {
					if (profileData.status === 401) {
						resolve(null);
					}
					else {
						throw new Error(
							`Failed retrieving user profile: ${JSON.stringify(profileData)}
							with token: ${token}`
						);
					}
				}
				resolve(profileData.data.profile);
			});
	});
};
