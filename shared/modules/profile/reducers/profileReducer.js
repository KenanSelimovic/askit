// @flow
"use strict";
import initialState from "../../../redux/defaultState";
import * as actionTypes from "../actions/types";
import {actionTypes as authActions} from "../../auth";

import type {ProfileActionType} from "../actions/profileActions.flow";
import type {ProfileDataType} from "../flowTypes/profile.flow";

export type ProfileInStateType = {|
	data: ?ProfileDataType
|};

// eslint-disable-next-line max-len
export default (state: ProfileInStateType = initialState.profile, action: ProfileActionType): ProfileInStateType => {
	switch (action.type) {
	case actionTypes.PROFILE_FETCH_SUCCESS:
		return Object.assign(
			{},
			state,
			{
				data: action.data
			}
		);
	case authActions.LOGOUT:
		return Object.assign(
			{},
			state,
			{
				data: null
			}
		);
	default:
		return state;
	}
};
