// @flow
"use strict";
import React from "react";
import ProfileInfo from "./ProfileInfo";
import ProfileEditForm from "./ProfileEditForm";
import PasswordChangeForm from "./PasswordChangeForm";

import type {ProfileDataType} from "../flowTypes/profile.flow";
type EditProfileViewPropsType = {
	data: ProfileDataType,
	editData: {
		email: string,
		first_name: string,
		last_name: string,
		password: string,
		password_second_time: string,
		profile_image_url: string,
		old_password: string
	},
	errors: {
		first_name: ?string,
		last_name: ?string,
		email: ?string,
		password: ?string,
		password_second_time: ?string,
		old_password: ?string
	},
	setError: (string, string) => void,
	removeError: (string) => Promise<void>,
	formFieldChange: (name: string, value: string) => void,
	submitProfileChange: () => void,
	submitPasswordChange: () => void,
	profileImageChange: (File) => void,
	profileFieldsChanged: boolean,
	passwordFieldsChanged: boolean
};

const EditProfileView = (props: EditProfileViewPropsType) => {
	const {
		data,
		editData
	} = props;
	const profileInfoData = Object.assign(
		{},
		props.data,
		{
			profile_image_url: editData.profile_image_url || data.profile_image_url
		}
	);
	const profileEditData = {
		first_name: editData.first_name,
		last_name: editData.last_name,
		email: editData.email
	};
	const passwordChangeData = {
		password: editData.password,
		password_second_time: editData.password_second_time,
		old_password: editData.old_password
	};
	return (
		<div className="edit-profile-view container">
			<ProfileInfo
				data={profileInfoData}
				profileImageChange={props.profileImageChange} />
			<ProfileEditForm
				data={profileEditData}
				errors={props.errors}
				setError={props.setError}
				removeError={props.removeError}
				formFieldChange={props.formFieldChange}
				submit={props.submitProfileChange}
				fieldsChanged={props.profileFieldsChanged} />
			<PasswordChangeForm
				data={passwordChangeData}
				errors={props.errors}
				setError={props.setError}
				removeError={props.removeError}
				formFieldChange={props.formFieldChange}
				submit={props.submitPasswordChange}
				fieldsChanged={props.passwordFieldsChanged} />
		</div>
	);
};
export default EditProfileView;
