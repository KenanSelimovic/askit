// @flow
"use strict";
import React from "react";
import TexInput from "../../common/reactComponents/TextInput";
import EmailInput from "../../common/reactComponents/EmailInput";
import {config} from "../../../config/";

if (process.env.CLIENT) {
	require("../styles/profileEditForm.css");
}

type ProfileEditFormPropsType = {|
	data: {
		first_name: string,
		last_name: string,
		email: string
	},
	errors: {
		first_name: ?string,
		last_name: ?string,
		email: ?string
	},
	setError: (string, string) => void,
	removeError: (string) => Promise<void>,
	formFieldChange: (name: string, value: string) => void,
	submit: () => void,
	fieldsChanged: boolean
|};

const ProfileEditForm = (props: ProfileEditFormPropsType) => {
	const {
		first_name_min_len,
		first_name_max_len,
		last_name_min_len,
		last_name_max_len
	} = config.users;
	return (
		<div className="profile-form profile-edit-form body-content-margin">
			<h3 className="title askit-blue">
				Edit profile data
			</h3>
			<TexInput
				title="First name"
				value={props.data.first_name || ""}
				fieldName="first_name"
				error={props.errors.first_name}
				setError={props.setError}
				removeError={props.removeError}
				required={false}
				onChange={props.formFieldChange}
				minLength={first_name_min_len}
				maxLength={first_name_max_len} />
			<TexInput
				title="Last name"
				value={props.data.last_name || ""}
				fieldName="last_name"
				error={props.errors.last_name}
				setError={props.setError}
				removeError={props.removeError}
				required={false}
				onChange={props.formFieldChange}
				minLength={last_name_min_len}
				maxLength={last_name_max_len} />
			<EmailInput
				title="Last name"
				value={props.data.email || ""}
				fieldName="email"
				error={props.errors.email}
				setError={props.setError}
				removeError={props.removeError}
				required={false}
				onChange={props.formFieldChange} />
			<button
				className="btn btn-default submit-button askit-blue-button"
				onClick={props.submit}
				disabled={!props.fieldsChanged}
			>
				Submit
			</button>
			<br style={{clear: "both"}} />
		</div>
	);
};
export default ProfileEditForm;
