// @flow
"use strict";
import React from "react";
import getFullUrl from "../../../helpers/getFullUrl";
import ProfileImage from "./ProfileImage";

import type {ProfileDataType} from "../flowTypes/profile.flow";
type ProfileInfoPropsType = {
	data: ProfileDataType,
	profileImageChange: (File) => void
};

const ProfileInfo = (props: ProfileInfoPropsType) => {
	const {
		data: {
			email,
			first_name,
			last_name,
			profile_image_url
		}
	} = props;
	const profileImageUrl = getFullUrl(profile_image_url);
	const padding = first_name && last_name ? " " : "";
	const fullName = (first_name || "") + padding + (last_name || "");
	return (
		<div className="profile-info">
			<div className="col-lg-2 col-md-3 col-sm-5 col-xs-12">
				<ProfileImage
					imgSrc={profileImageUrl}
					onChange={props.profileImageChange} />
			</div>
			<div className="col-lg-3 col-md-4 col-sm-5 col-xs-12 user-summary">
				<p className="full-name">
					{fullName || "Profile"}
				</p>
				<p className="email">
					{email}
				</p>
			</div>
			<br style={{clear: "both"}} />
		</div>
	);
};
export default ProfileInfo;
