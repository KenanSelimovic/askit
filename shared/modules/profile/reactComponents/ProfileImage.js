// @flow
"use strict";
import React from "react";
import toastr from "toastr";

if (process.env.CLIENT) {
	require("../styles/profileImage.css");
}

type ProfileImagePropsType = {
	imgSrc: string,
	onChange: (File) => void
};

class ProfileImage extends React.PureComponent {
	props: ProfileImagePropsType;
	onChange = (e: SyntheticInputEvent) => {
		const imageFile = e.target.files[0];
		if (imageFile && imageFile.size >= 1500000) {
			toastr.error("Max image size is 1.5MB");
			return;
		}
		this.props.onChange(imageFile);
	};
	onClick = (e: SyntheticInputEvent) => {
		e.target.value = "";
	};
	render() {
		const {
			imgSrc
		} = this.props;
		const disabled = this.props.disabled || false;
		return (
			<div className="profile-image">
				<img
					src={imgSrc}
					className="user-image" />
				<div className="upload-button btn btn-default">
					Upload image
					<input
						type="file"
						accept="image/*"
						onChange={this.onChange}
						disabled={disabled}
						onClick={this.onClick} />
				</div>
			</div>);
	}
}
export default ProfileImage;
