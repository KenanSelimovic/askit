// @flow
"use strict";
import React from "react";
import PasswordInput from "../../common/reactComponents/PasswordInput";
import {config} from "../../../config/";

if (process.env.CLIENT) {
	require("../styles/changePassword.css");
}

type ProfileEditFormPropsType = {|
	data: {
		password: string,
		password_second_time: string,
		old_password: string
	},
	errors: {
		password: ?string,
		password_second_time: ?string,
		old_password: ?string
	},
	setError: (string, string) => void,
	removeError: (string) => Promise<void>,
	formFieldChange: (name: string, value: string) => void,
	submit: () => void,
	fieldsChanged: boolean
|};

const ProfileEditForm = (props: ProfileEditFormPropsType) => {
	const {
		password_min_len,
		password_max_len
	} = config.users;
	return (
		<div className="profile-form password-change-form body-content-margin">
			<h3 className="title askit-blue">
				Change password
			</h3>
			<PasswordInput
				title="Current password"
				value={props.data.old_password || ""}
				fieldName="old_password"
				error={props.errors.old_password}
				setError={props.setError}
				removeError={props.removeError}
				required={true}
				onChange={props.formFieldChange}
				minLength={password_min_len}
				maxLength={password_max_len} />
			<PasswordInput
				title="New password"
				value={props.data.password || ""}
				fieldName="password"
				error={props.errors.password}
				setError={props.setError}
				removeError={props.removeError}
				required={true}
				onChange={props.formFieldChange}
				minLength={password_min_len}
				maxLength={password_max_len}
				repeatedValue={props.data.password_second_time} />
			<PasswordInput
				title="Reenter new password"
				value={props.data.password_second_time || ""}
				fieldName="password_second_time"
				error={props.errors.password_second_time}
				setError={props.setError}
				removeError={props.removeError}
				required={true}
				onChange={props.formFieldChange}
				minLength={password_min_len}
				maxLength={password_max_len}
				repeatedValue={props.data.password} />
			<button
				className="btn btn-default submit-button askit-blue-button"
				onClick={props.submit}
				disabled={!props.fieldsChanged}
			>
				Change password
			</button>
			<br style={{clear: "both"}} />
		</div>
	);
};
export default ProfileEditForm;
