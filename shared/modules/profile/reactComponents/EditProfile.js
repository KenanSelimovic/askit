// @flow
"use strict";
import React from "react";
import EditProfileView from "./EditProfileView";
import {getUserProfile} from "../selectors/profileSelectors";
import type {GlobalAppStateType} from "../../../redux/defaultState";
import {connect} from "react-redux";
import turnIntoForm from "../../common/reactComponents/turnIntoForm";
import {baseUrl} from "../../../config/";
import {selectors as authSelectors} from "../../auth/";
import logError from "../../../../client/modules/logging/errorLogger";
import {fetchProfile} from "../actions/profileActions";
import toastr from "toastr";
import {requireAuth} from "../../auth/";
import {config} from "../../../config";

if (process.env.CLIENT) {
	require("../styles/editProfile.css");
}

import type {ProfileDataType} from "../flowTypes/profile.flow";
import type {ProfileEditResponseType} from "../flowTypes/apiResponses.flow";
type EditProfilePropsType = {
	data: ProfileDataType,
	checkIfAnyErrors: (Array<string>) => boolean,
	setError: (string, string) => void,
	removeError: (string) => Promise<void>,
	resetAllErrors: () => Promise<void>,
	errors: {
		email: ?string,
		first_name: ?string,
		last_name: ?string,
		password: ?string,
		password_second_time: ?string,
		old_password: ?string
	},
	addTrackedField: (name: string, required: ?boolean, alreadyValid: ?boolean) => void,
	token: string,
	refetchProfile: (token: string) => void
};
type EditProfileStateType = {
	profileFieldsChanged: boolean,
	passwordFieldsChanged: boolean,
	editData: {
		email: string,
		first_name: string,
		last_name: string,
		password: string,
		password_second_time: string,
		profile_image_url: string,
		old_password: string
	},
	profileImageUpload: {
		newImage: ?File
	}
};

class EditProfile extends React.PureComponent {
	props: EditProfilePropsType;
	state: EditProfileStateType = {
		profileFieldsChanged: false,
		passwordFieldsChanged: false,
		editData: {
			email: "",
			first_name: "",
			last_name: "",
			password: "",
			password_second_time: "",
			profile_image_url: "",
			old_password: ""
		},
		profileImageUpload: {
			newImage: null
		}
	};
	componentDidMount = () => {
		this.addProfileToState();
		this.startTrackingForm();
	};
	addProfileToState = () => {
		const {
			email,
			first_name,
			last_name
		} = this.props.data;
		this.setState(() => ({
			editData: {
				email,
				first_name,
				last_name
			}
		}));
	};
	startTrackingForm = () => {
		const fields = [
			{name: "first_name", required: false, valid: true},
			{name: "last_name", required: false, valid: true},
			{name: "email", required: true, valid: true},
			{name: "password", required: true, valid: false},
			{name: "password_second_time", required: true},
			{name: "old_password", required: true}
		];
		fields.forEach(field => {
			this.props.addTrackedField(
				field.name,
				field.required,
				field.valid || false
			);
		});
	};
	formFieldChange = (fieldType: string, value: string) => {
		const {
			passwordFieldsChanged,
			profileFieldsChanged
		} = this.state;

		// unclock form submit button now that user has made a change
		const isPasswordField = fieldType.indexOf("password") !== -1;
		if (isPasswordField && !passwordFieldsChanged) {
			this.setState(() => ({passwordFieldsChanged: true}));
		}
		else if (!isPasswordField && !profileFieldsChanged) {
			this.setState(() => ({profileFieldsChanged: true}));
		}

		// store change in state
		this.setState((prevState: EditProfileStateType) => ({
			editData: Object.assign(
				{},
				prevState.editData,
				{
					[fieldType]: value
				}
			)
		}));
	};
	submitPasswordChange = () => {
		const fieldsOfInterest = [
			"password",
			"password_second_time",
			"old_password"
		];
		if (this.props.checkIfAnyErrors(fieldsOfInterest)) {
			return;
		}
		const {
			password,
			old_password
		} = this.state.editData;
		const profileChangeData = {
			new_password: password,
			old_password
		};

		fetch(`${baseUrl}/v1/auth/password`, {
			method: "POST",
			headers: {
				"Content-type": "application/json",
				authorization: this.props.token
			},
			body: JSON.stringify({data: profileChangeData})
		})
			.then(unparsed => unparsed.json())
			.then((editResponse: ProfileEditResponseType) => {
				if (editResponse.status !== 200) {
					if (editResponse.code === 16) {
						toastr.warning("Incorrect current password");
					}
					else {
						logError(
							new Error(
								`Failed changing password: ${JSON.stringify(editResponse)}
								with data: ${JSON.stringify(profileChangeData)}`
							)
						);
						toastr.error("Unknown error occurred");
					}
					return;
				}
				toastr.remove();
				toastr.success("Password changed");
				this.setState((prevState: EditProfileStateType) => ({
					passwordFieldsChanged: false,
					editData: Object.assign(
						{},
						prevState.editData,
						{
							password: "",
							password_second_time: "",
							old_password: ""
						}
					)
				}));
			});
	};
	submitProfileEdit = () => {
		const {editData} = this.state;
		const fieldsOfInterest = [
			"first_name",
			"last_name",
			"email"
		];
		if (this.props.checkIfAnyErrors(fieldsOfInterest)) {
			return;
		}
		const {
			first_name,
			last_name,
			email
		} = this.state.editData;
		const profileUpdateData = {
			first_name,
			last_name,
			email
		};
		this.uploadProfileImage()
			.then(() => {
				fetch(`${baseUrl}/v1/users/profile`, {
					method: "POST",
					headers: {
						"Content-type": "application/json",
						authorization: this.props.token
					},
					body: JSON.stringify({data: profileUpdateData})
				})
					.then(unparsed => unparsed.json())
					.then((editResponse: ProfileEditResponseType) => {
						if (editResponse.status !== 200) {
							if (editResponse.code === 2) {
								toastr.warning("Email already in use");
							}
							else {
								logError(
									new Error(
										`Failed editing profile: ${JSON.stringify(editResponse)}
										with data: ${JSON.stringify(editData)}`
									)
								);
								toastr.error("Unknown error occurred");
							}
							return;
						}
						this.setState(() => ({profileFieldsChanged: false}));
						this.props.refetchProfile(this.props.token);
						toastr.success("Profile updated");
					});
			})
			.catch(err => {
				logError(err);
				toastr.error("Unknown error occurred");
			});
	};
	uploadProfileImage = (): Promise<void> => {
		return new Promise((resolve) => {
			const profileImage = this.state.profileImageUpload.newImage;
			if (!profileImage) {
				resolve();
				return;
			}
			let formData = new FormData();
			formData.append("profile_image", profileImage);
			fetch(`${baseUrl}/v1/users/image`, {
				method: "POST",
				headers: {
					authorization: this.props.token
				},
				body: formData
			})
				.then(unparsedImageUploadResponse => unparsedImageUploadResponse.json())
				.then((imageUploadResponse) => {
					if (imageUploadResponse.status !== 200) {
						if (imageUploadResponse.code === 6) {
							toastr.warning("Please select a valid image file");
						}
						else {
							logError(
								new Error(
									`Image upload failed: ${JSON.stringify(imageUploadResponse)}`
								)
							)
							toastr.error("Image upload failed");
						}
						return;
					}
					resolve();
				});
		});
	};
	profileImageChange = (imageFile: File) => {
		this.setState({
			profileImageUpload: Object.assign(
				this.state.profileImageUpload,
				{
					newImage: imageFile
				}
			)
		});
		const uploadNow: boolean = config.users.upload_profile_image_on_select;
		if (uploadNow) {
			this.uploadProfileImage()
				.then(() => {
					toastr.success("Profile image updated");
					this.props.refetchProfile(this.props.token);
				});
		}
		else {
			// preview the image
			const reader = new FileReader();
			const self = this;
			// eslint-disable-next-line immutable/no-mutation
			reader.onload = (readerEvent: Event) => {
				self.setState({
					editData: Object.assign(
						{},
						this.state.editData,
						{
							profile_image_url: readerEvent.target.result
						}
					)
				});
			};
			reader.readAsDataURL(imageFile);
		}
	};
	render() {
		const {editData} = this.state;
		return (
			<div className="edit-profile">
				<EditProfileView
					data={this.props.data}
					editData={editData}
					errors={this.props.errors}
					setError={this.props.setError}
					removeError={this.props.removeError}
					formFieldChange={this.formFieldChange}
					profileImageChange={this.profileImageChange}
					submitProfileChange={this.submitProfileEdit}
					submitPasswordChange={this.submitPasswordChange}
					passwordFieldsChanged={this.state.passwordFieldsChanged}
					profileFieldsChanged={this.state.profileFieldsChanged} />
			</div>
		);
	}
}
const mapStateToProps = (state: GlobalAppStateType) => ({
	data: getUserProfile(state),
	token: authSelectors.getToken(state)
});
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	refetchProfile: (token: string) => {
		dispatch(fetchProfile(token));
	}
});
const ConnectedEditProfile = connect(mapStateToProps, mapDispatchToProps)(EditProfile);
const ProtectedEditProfile = requireAuth(ConnectedEditProfile);
export default turnIntoForm(ProtectedEditProfile);
