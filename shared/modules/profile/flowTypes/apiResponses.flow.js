// @flow
"use strict";
import type {ProfileDataType} from "./profile.flow";

export type GetProfileResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		profile: ProfileDataType
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};
export type ProfileEditResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		id: number
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};
