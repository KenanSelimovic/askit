// @flow
"use strict";
import type {VoteDataType} from "../../votes/";
export type ProfileDataType = {|
	id: number,
	email: string,
	first_name: ?string,
	last_name: ?string,
	profile_image_url: ?string,
	votes: Array<VoteDataType>
|};
