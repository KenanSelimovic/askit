// @flow
"use strict";
import type {ProfileDataType} from "../flowTypes/profile.flow";

export type ProfileFetchSuccessActionType = {
	type: "PROFILE_FETCH_SUCCESS",
	data: ProfileDataType
};

export type ProfileActionType = ProfileFetchSuccessActionType;
