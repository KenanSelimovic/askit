// @flow
"use strict";
import * as actionTypes from "./types";
import getProfile from "../methods/getProfile";
// import logError from "../../../../client/modules/logging/errorLogger";

// flow types
import type {Dispatch} from "redux";
import type {ProfileDataType} from "../flowTypes/profile.flow";
import type {ProfileFetchSuccessActionType} from "./profileActions.flow";

export const profileFetchSuccess = (data: ProfileDataType): ProfileFetchSuccessActionType => ({
	type: actionTypes.PROFILE_FETCH_SUCCESS,
	data
});
export const fetchProfile = (token: string) => {
	return (dispatch: Dispatch<*>) => {
		return getProfile(token)
			.then((userProfile: ?ProfileDataType) => {
				if (!userProfile) {
					throw new Error(
						`Profile fetch failed for token: ${token}`
					);
				}
				dispatch(profileFetchSuccess(userProfile));
			});
	};
};
