// @flow
"use strict";
import type {GlobalAppStateType} from "../../../redux/defaultState";

export const getUserProfile = (state: GlobalAppStateType) => state.profile.data;
