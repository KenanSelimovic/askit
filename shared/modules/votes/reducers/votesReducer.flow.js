// @flow
"use strict";
import initialState from "../../../redux/defaultState";
import * as actionTypes from "../actions/types";
import type {VotesActionType} from "../actions/votesActions.flow";


export type VotesInStateType = {|
	userVotes: []
|};

// eslint-disable-next-line max-len
export default (state: VotesInStateType = initialState.votes, action: VotesActionType): VotesInStateType => {
	switch (action.type) {
	case actionTypes.VOTES_LOAD_SUCCESS: {
		return Object.assign(
			{},
			state,
			{
				userVotes: action.data.votes
			}
		);
	}
	default:
		return state;
	}
};
