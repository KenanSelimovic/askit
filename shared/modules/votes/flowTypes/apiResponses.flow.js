// @flow
"use strict";
import type {VoteDataType} from "./votes.flow";

export type UserVotesResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		votes: Array<VoteDataType>
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};
