// @flow
"use strict";
export type VoteType = "up" | "down";
export type VoteDataType = {|
	post_id: number,
	type: VoteType,
	user: {
		id: number
	}
|};
