// @flow
"use strict";
import type {GlobalAppStateType} from "../../../redux/defaultState";

export const getUserVotes = (state: GlobalAppStateType) => {
	return state.votes.userVotes;
};
