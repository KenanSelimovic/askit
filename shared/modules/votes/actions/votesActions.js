// @flow
"use strict";
import * as actionTypes from "./types";
import {baseUrl} from "../../../config/";
import toastr from "toastr";
import logError from "../../../../client/modules/logging/errorLogger";

import type {
	VotesActionType
} from "./votesActions.flow";
import type {UserVotesResponseType} from "../flowTypes/apiResponses.flow";
import type {VoteDataType} from "../flowTypes/votes.flow";
import type {Dispatch} from "redux";

export const votesLoadSuccess = (
	votes: Array<VoteDataType>
) => ({
	type: actionTypes.VOTES_LOAD_SUCCESS,
	data: {
		votes
	}
});

export const loadUserVotes = (token: string) => {
	return (dispatch: Dispatch<VotesActionType>) => {
		const fetchUrl = `${baseUrl}/v1/votes`;
		return fetch(fetchUrl, {
			headers: {
				authorization: token
			}
		})
			.then(unparsed => unparsed.json())
			.then((votesResponse: UserVotesResponseType) => {
				if (!votesResponse.data) {
					logError(
						new Error(
							`Failed retrieving user votes: ${JSON.stringify(votesResponse)}`
						)
					);
					toastr.error("Unknown error occurred");
					return;
				}
				dispatch(votesLoadSuccess(
					votesResponse.data.votes
				));
			})
			.catch(err => {
				logError(err);
				toastr.error("Unknown error occurred");
			});
	};
};
