// @flow
"use strict";
import type {VoteDataType} from "../flowTypes/votes.flow";

export type VotesLoadSuccessActionType = {|
	type: "VOTES_LOAD_SUCCESS",
	data: {
		votes: Array<VoteDataType>
	}
|};
export type VotesActionType = VotesLoadSuccessActionType;
