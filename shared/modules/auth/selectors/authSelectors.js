// @flow
"use strict";
import type {GlobalAppStateType} from "../../../redux/defaultState";

export const getLoginStatus = (state: GlobalAppStateType) => !!state.auth.token;
export const getToken = (state: GlobalAppStateType) => state.auth.token;
export const getUserId = (state: GlobalAppStateType) => state.auth.id;
