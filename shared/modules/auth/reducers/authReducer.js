// @flow
"use strict";
import * as actionTypes from "../actions/types";
import defaultState from "../../../redux/defaultState";

import type {AuthActionType} from "../actions/authActions.flow";

export type AuthStateType = {|
	id: ?number,
	token: ?string
|};

// eslint-disable-next-line max-len
export default (state: AuthStateType = defaultState.auth, action: AuthActionType): AuthStateType => {
	switch (action.type) {
	case actionTypes.LOGIN_SUCCESS:
		return Object.assign(
			{},
			state,
			{
				token: action.data.token,
				id: action.data.id
			}
		);
	case actionTypes.LOGOUT:
		return Object.assign(
			{},
			state,
			{
				token: null,
				id: null
			}
		);
	default:
		return state;
	}
};
