// @flow
"use strict";
import React from "react";
import RegistrationForm from "./RegistrationForm";
import turnIntoForm from "../../common/reactComponents/turnIntoForm";
import type {RegistrationDataType} from "../flowTypes/auth.flow";
import {baseUrl} from "../../../config/index";
import toastr from "toastr";
import logError from "../../../../client/modules/logging/errorLogger";
import * as authActions from "../actions/authActions";
import {connect} from "react-redux";
import {actions as profileActions} from "../../profile/";

if (process.env.CLIENT) {
	require("../styles/register.css");
}

import type {RegistrationResponseType} from "../flowTypes/apiResponses.flow";
type RegisterStateType = {
	data: RegistrationDataType
};
type RegisterPropsType = {
	checkIfAnyErrors: (Array<string>) => boolean,
	setError: (string, string) => void,
	removeError: (string) => Promise<void>,
	resetAllErrors: () => Promise<void>,
	errors: {
		email: ?string,
		first_name: ?string,
		last_name: ?string,
		password: ?string,
		password_second_time: ?string
	},
	addTrackedField: (string, ?boolean, ?boolean) => void,
	router: {
		push: (string) => void
	},
	location: {
		search: string
	},
	loginSuccess: (string, number) => void,
	fetchProfile: (token: string) => void
};

class Register extends React.PureComponent {
	props: RegisterPropsType;
	state: RegisterStateType = {
		data: {
			email: "",
			first_name: "",
			last_name: "",
			password: "",
			password_second_time: ""
		}
	};
	componentDidMount = () => {
		this.startTrackingForm();
	};
	startTrackingForm = () => {
		const fields = [
			{name: "first_name", required: false, valid: false},
			{name: "last_name", required: false, valid: false},
			{name: "email", required: true, valid: false},
			{name: "password", required: true, valid: false},
			{name: "password_second_time", required: true, valid: false}
		];
		fields.forEach(field => {
			this.props.addTrackedField(
				field.name,
				field.required,
				field.valid || false
			);
		});
	};
	formInputChange = (fieldType: string, value: string) => {
		this.setState((prevState: RegisterStateType) => ({
			data: Object.assign(
				{},
				prevState.data,
				{
					[fieldType]: value
				}
			)
		}));
	};
	submit = () => {
		const fieldsOfInterest = [
			"email",
			"password",
			"password_second_time",
			"first_name",
			"last_name"
		];
		if (this.props.checkIfAnyErrors(fieldsOfInterest)) {
			return;
		}
		const submissionUrl = `${baseUrl}/v1/auth/register`;
		fetch(submissionUrl, {
			method: "POST",
			headers: {
				"Content-type": "application/json"
			},
			body: JSON.stringify({data: this.state.data})
		})
			.then(unparsed => unparsed.json())
			.then((registrationResponse: RegistrationResponseType) => {
				if (registrationResponse.status !== 200 || !registrationResponse.data) {
					if (registrationResponse.code === 2) {
						toastr.warning("Email already in use");
					}
					else {
						logError(
							new Error(
								`Registration failed: ${JSON.stringify(registrationResponse)}`
							)
						);
					}
					return;
				}
				this.props.loginSuccess(
					registrationResponse.data.token,
					registrationResponse.data.id
				);
				this.props.fetchProfile(
					registrationResponse.data.token
				);
				this.props.router.push("/");
			})
			.catch(err => {
				logError(err);
				toastr.error("Unknown error occurred");
			});
	};
	render() {
		return (
			<div className="register container">
				<h2 className="askit-blue">
					Register
				</h2>
				<RegistrationForm
					data={this.state.data}
					setError={this.props.setError}
					removeError={this.props.removeError}
					errors={this.props.errors}
					formInputChange={this.formInputChange}
					submit={this.submit} />
			</div>
		);
	}
}
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	loginSuccess: (token: string, userId: number) => {
		dispatch(authActions.loginSuccess({token, id: userId}));
	},
	fetchProfile: (token: string) => {
		dispatch(profileActions.fetchProfile(token));
	}
});
const ConnectedRegister = connect(null, mapDispatchToProps)(Register);
export default turnIntoForm(ConnectedRegister);
