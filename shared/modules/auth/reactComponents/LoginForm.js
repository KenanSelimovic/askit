// @flow
"use strict";
import React from "react";
import EmailInput from "./../../common/reactComponents/EmailInput";
import PasswordInput from "./../../common/reactComponents/PasswordInput";
import {config} from "../../../config/index";

const usersConfig = config.users;

import type {LoginDataType} from "../flowTypes/auth.flow";
type LoginFormPropsType = {
	data: LoginDataType,
	setError: (string, string) => void,
	removeError: (string) => Promise<void>,
	errors: {
		email: ?string,
		password: ?string
	},
	formInputChange: (field: string, value: string) => void,
	submit: () => void
};

const LoginForm = (props: LoginFormPropsType) => {
	return (
		<div className="registration-form row">
			<EmailInput
				fieldName="email"
				setError={props.setError}
				removeError={props.removeError}
				error={props.errors.email}
				value={props.data.email}
				onChange={props.formInputChange} />
			<PasswordInput
				fieldName="password"
				required={false}
				minLength={usersConfig.password_min_len}
				maxLength={usersConfig.password_max_len}
				setError={props.setError}
				removeError={props.removeError}
				error={props.errors.password}
				value={props.data.password}
				onChange={props.formInputChange}
				title="Password" />
			<div className="submit-button">
				<button
					className="btn btn-default submit-registration askit-blue-button"
					onClick={props.submit}
				>
					Submit
				</button>
			</div>
		</div>
	);
};
export default LoginForm;
