// @flow
"use strict";
import React from "react";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import {getLoginStatus} from "../selectors/authSelectors";

// flow types
import type {GlobalAppStateType} from "../../../redux/defaultState";
type AuthenticatedComponentPropsType = {
	isAuthenticated: boolean,
	router: {
		push: (string) => void
	}
};

export default (ComponentToWrap: any) => {
	const AuthenticatedComponent = (props: AuthenticatedComponentPropsType) => {
		if (!props.isAuthenticated) {
			props.router.push(`/login`);
			return null;
		}
		return (<ComponentToWrap {...props}/>);
	};

	const AuthenticatedComponentWithRouter = withRouter(AuthenticatedComponent);

	const mapStateToProps = (state: GlobalAppStateType) => ({
		isAuthenticated: getLoginStatus(state)
	});
	return connect(mapStateToProps)(AuthenticatedComponentWithRouter);
};
