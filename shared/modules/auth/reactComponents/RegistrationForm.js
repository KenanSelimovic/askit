// @flow
"use strict";
import React from "react";
import EmailInput from "./../../common/reactComponents/EmailInput";
import TextInput from "./../../common/reactComponents/TextInput";
import PasswordInput from "./../../common/reactComponents/PasswordInput";
import {config} from "../../../config/index";

const usersConfig = config.users;

import type {RegistrationDataType} from "../flowTypes/auth.flow";
type RegistrationFormPropsType = {
	data: RegistrationDataType,
	setError: (string, string) => void,
	removeError: (string) => Promise<void>,
	errors: {
		email: ?string,
		first_name: ?string,
		last_name: ?string,
		password: ?string,
		password_second_time: ?string
	},
	formInputChange: (field: string, value: string) => void,
	submit: () => void
};

const RegistrationForm = (props: RegistrationFormPropsType) => {
	return (
		<div className="registration-form row">
			<EmailInput
				fieldName="email"
				setError={props.setError}
				removeError={props.removeError}
				error={props.errors.email}
				value={props.data.email}
				onChange={props.formInputChange} />
			<TextInput
				fieldName="first_name"
				required={false}
				minLength={usersConfig.first_name_min_len}
				maxLength={usersConfig.first_name_max_len}
				setError={props.setError}
				removeError={props.removeError}
				error={props.errors.first_name}
				value={props.data.first_name}
				onChange={props.formInputChange}
				title="First name" />
			<TextInput
				fieldName="last_name"
				required={false}
				minLength={usersConfig.last_name_min_len}
				maxLength={usersConfig.last_name_max_len}
				setError={props.setError}
				removeError={props.removeError}
				error={props.errors.last_name}
				value={props.data.last_name}
				onChange={props.formInputChange}
				title="Last name" />
			<PasswordInput
				fieldName="password"
				required={false}
				minLength={usersConfig.password_min_len}
				maxLength={usersConfig.password_max_len}
				setError={props.setError}
				removeError={props.removeError}
				error={props.errors.password}
				value={props.data.password}
				onChange={props.formInputChange}
				title="Password"
				repeatedValue={props.data.password_second_time} />
			<PasswordInput
				fieldName="password_second_time"
				required={false}
				minLength={usersConfig.password_min_len}
				maxLength={usersConfig.password_max_len}
				setError={props.setError}
				removeError={props.removeError}
				error={props.errors.password_second_time}
				value={props.data.password_second_time}
				onChange={props.formInputChange}
				title="Repeat password"
				repeatedValue={props.data.password} />
			<div className="submit-button">
				<button
					className="btn btn-default submit-registration askit-blue-button"
					onClick={props.submit}
				>
					Submit
				</button>
			</div>
		</div>
	);
};
export default RegistrationForm;
