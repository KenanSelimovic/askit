// @flow
"use strict";
import React from "react";
import LoginForm from "./LoginForm";
import turnIntoForm from "../../common/reactComponents/turnIntoForm";
import type {LoginDataType} from "../flowTypes/auth.flow";
import {baseUrl} from "../../../config/index";
import toastr from "toastr";
import logError from "../../../../client/modules/logging/errorLogger";
import * as authActions from "../actions/authActions";
import {connect} from "react-redux";
import {actions as profileActions} from "../../profile/";

if (process.env.CLIENT) {
	require("../styles/register.css");
}

import type {LoginResponseType} from "../flowTypes/apiResponses.flow";
type LoginStateType = {
	data: LoginDataType
};
type LoginPropsType = {
	checkIfAnyErrors: (Array<string>) => boolean,
	setError: (string, string) => void,
	removeError: (string) => Promise<void>,
	resetAllErrors: () => Promise<void>,
	errors: {
		email: ?string,
		password: ?string
	},
	addTrackedField: (string, ?boolean, ?boolean) => void,
	router: {
		push: (string) => void
	},
	location: {
		search: string
	},
	loginSuccess: (token: string, id: number) => void,
	fetchProfile: (token: string) => void
};

class Login extends React.PureComponent {
	props: LoginPropsType;
	state: LoginStateType = {
		data: {
			email: "",
			password: ""
		}
	};
	componentDidMount = () => {
		this.startTrackingForm();
	};
	startTrackingForm = () => {
		const fields = [
			{name: "email", required: true, valid: false},
			{name: "password", required: true, valid: false}
		];
		fields.forEach(field => {
			this.props.addTrackedField(
				field.name,
				field.required,
				field.valid || false
			);
		});
	};
	formInputChange = (fieldType: string, value: string) => {
		this.setState((prevState: LoginStateType) => ({
			data: Object.assign(
				{},
				prevState.data,
				{
					[fieldType]: value
				}
			)
		}));
	};
	submit = () => {
		const fieldsOfInterest = [
			"email",
			"password"
		];
		if (this.props.checkIfAnyErrors(fieldsOfInterest)) {
			return;
		}
		const submissionUrl = `${baseUrl}/v1/auth/login`;
		fetch(submissionUrl, {
			method: "POST",
			headers: {
				"Content-type": "application/json"
			},
			body: JSON.stringify({data: this.state.data})
		})
			.then(unparsed => unparsed.json())
			.then((loginResponse: LoginResponseType) => {
				if (loginResponse.status !== 200 || !loginResponse.data) {
					if (loginResponse.code === 16) {
						toastr.remove();
						toastr.warning("Incorrect credentials");
					}
					else {
						toastr.remove();
						toastr.error("Unknown error occurred");
						logError(
							new Error(
								`Login failed: ${JSON.stringify(loginResponse)}`
							)
						);
					}
					return;
				}
				this.props.loginSuccess(
					loginResponse.data.token,
					loginResponse.data.id
				);
				this.props.fetchProfile(loginResponse.data.token);
				this.props.router.push("/");
			})
			.catch(err => {
				logError(err);
				toastr.error("Unknown error occurred");
			});
	};
	render() {
		return (
			<div className="register container">
				<h2 className="askit-blue">
					Login
				</h2>
				<LoginForm
					data={this.state.data}
					setError={this.props.setError}
					removeError={this.props.removeError}
					errors={this.props.errors}
					formInputChange={this.formInputChange}
					submit={this.submit} />
			</div>
		);
	}
}
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	loginSuccess: (token: string, userId: number) => {
		dispatch(authActions.loginSuccess({token, id: userId}));
	},
	fetchProfile: (token: string) => {
		dispatch(profileActions.fetchProfile(token));
	}
});
const ConnectedLogin = connect(null, mapDispatchToProps)(Login);
export default turnIntoForm(ConnectedLogin);
