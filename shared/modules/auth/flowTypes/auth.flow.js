// @flow
"use strict";
export type RegistrationDataType = {|
	first_name: string,
	last_name: string,
	email: string,
	password: string,
	password_second_time: string
|};
export type LoginDataType = {|
	email: string,
	password: string
|};
export type LoginResponseDataType = {|
	token: string,
	id: number
|};
export type LoginSubmitDataType = {|
	email: string,
	password: string
|};
