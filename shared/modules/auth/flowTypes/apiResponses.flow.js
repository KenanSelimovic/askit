// @flow
"use strict";
export type RegistrationResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		id: number,
		token: string
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};
export type LoginResponseType = {|
	status: 200,
	code: null,
	message: string,
	data: {
		id: number,
		token: string
	}
|}
|
{|
	status: number,
	code: number,
	message: string,
	data: null
|};