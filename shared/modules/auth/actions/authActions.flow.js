// @flow
"use strict";
import type {
	LoginResponseDataType
} from "../flowTypes/auth.flow";

export type LoginSuccessActionType = {
	type: "LOGIN_SUCCESS",
	data: LoginResponseDataType
};
export type LogoutActionType = {
	type: "LOGOUT"
};
export type AuthActionType =
	LoginSuccessActionType
	| LogoutActionType;
