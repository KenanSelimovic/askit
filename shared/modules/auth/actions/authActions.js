// @flow
"use strict";
import cookie from "react-cookie";
import * as actionTypes from "./types";
import type {
	LoginResponseDataType
} from "../flowTypes/auth.flow";
import type {LoginSuccessActionType, LogoutActionType} from "./authActions.flow";

export const loginSuccess = (data: LoginResponseDataType): LoginSuccessActionType => {
	const {token, id} = data;
	cookie.save("askitUserId", id, {path: "/"});
	cookie.save("askitToken", token, {path: "/"});
	return ({
		type: actionTypes.LOGIN_SUCCESS,
		data: {
			token,
			id
		}
	});
};
export const logout = (): LogoutActionType => {
	cookie.remove("askitToken", {path: "/"});
	cookie.remove("askitUserId", {path: "/"});
	return ({
		type: actionTypes.LOGOUT
	});
};
