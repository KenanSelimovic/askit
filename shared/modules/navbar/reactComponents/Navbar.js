// @flow
"use strict";
import React from "react";
import {Link} from "react-router";
import {connect} from "react-redux";
import {selectors as authSelectors, actions as authActions} from "../../auth";

if (process.env.CLIENT) {
	require("../styles/navbar.css");
}

import type {GlobalAppStateType} from "../../../redux/defaultState";
type NavbarPropsType = {
	isAuthenticated: boolean,
	logout: () => void
};

const Navbar = (props: NavbarPropsType) => {
	const {
		isAuthenticated
	} = props;
	return (
		<div className="navbar">
			<div className="btn-group" role="group" aria-label="...">
				<Link to="/">
					<button type="button" className="btn">
						Home
					</button>
				</Link>
				{isAuthenticated &&
				<Link to="/profile">
					<button type="button" className="btn">
						Profile
					</button>
				</Link>}
				{isAuthenticated &&
				<Link to="/my-questions">
					<button type="button" className="btn">
						My questions
					</button>
				</Link>}
				{!isAuthenticated &&
				<Link to="/login">
					<button type="button" className="btn">
						Login
					</button>
				</Link>}
				{!isAuthenticated &&
				<Link to="/register">
					<button type="button" className="btn">
						Register
					</button>
				</Link>}
				{isAuthenticated &&
				<Link to="/" onClick={props.logout}>
					<button type="button" className="btn">
						Logout
					</button>
				</Link>}
			</div>
		</div>
	);
};

const mapStateToProps = (state: GlobalAppStateType) => ({
	isAuthenticated: authSelectors.getLoginStatus(state)
});
const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
	logout: () => {
		dispatch(authActions.logout());
	}
});
export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
