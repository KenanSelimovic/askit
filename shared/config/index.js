// @flow
"use strict";
type GlobalConfigType = {
	port: number,
	server_url: string,
	users: {
		default_username: string,
		default_image: string,
		first_name_min_len: number,
		first_name_max_len: number,
		last_name_min_len: number,
		last_name_max_len: number,
		password_min_len: number,
		password_max_len: number,
		upload_profile_image_on_select: boolean
	},
	questions: {
		questions_per_page: number,
		text_min_len: number,
		text_max_len: number,
		search_text_min_len: number,
		search_text_max_len: number
	},
	answers: {
		text_min_len: number,
		text_max_len: number,
		answers_per_page: number
	}
};

const defaultEnvironment = "development";
const environment = process.env.NODE_ENV || defaultEnvironment;

const globalConfig: GlobalConfigType = require("../../config.json")[environment];

module.exports.environment = environment;

// docker conf - set different url for client, as it's connecting
// outside of docker-compose network
module.exports.baseUrl = process.env.SERVER_URL || globalConfig.server_url;

module.exports.config = globalConfig;
module.exports.defaultProfileImage = globalConfig.users.default_image;