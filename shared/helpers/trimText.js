// @flow
"use strict";

export default (text: string, maxLength: number): string => {
	return text.length > maxLength
		? `${text.substring(0, maxLength - 3)}...`
		: text;
};
