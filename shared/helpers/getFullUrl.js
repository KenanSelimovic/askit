// @flow
"use strict";
import {config, defaultProfileImage} from "../config/index";

export default (unparsedUrl: ?string): string => {
	if (!unparsedUrl) {
		return defaultProfileImage;
	}
	if (unparsedUrl.indexOf("data:image") === 0) {
		return unparsedUrl;
	}
	const serverUrl = config.server_url.replace("/api", "");
	// as images will always get rendered only on the client side,
	// we read server url directly from config in order to avoid getting
	// server version of the url
	return unparsedUrl.indexOf("http") !== -1 ? unparsedUrl : `${serverUrl}${unparsedUrl}`;
};
