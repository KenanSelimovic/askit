// @flow
"use strict";

const toSeconds = (milliseconds: number): number => {
	return Math.round(milliseconds / 1000);
};
const toMinutes = (milliseconds: number): number => {
	return Math.round(toSeconds(milliseconds) / 60);
};
const toHours = (milliseconds: number): number => {
	return Math.round(toMinutes(milliseconds) / 60);
};
const toDays = (milliseconds: number): number => {
	return Math.round(toHours(milliseconds) / 24);
};
const toExactDate = (milliseconds: number): string => {
	const {languages, language, userLanguage} = window.navigator;
	const userLocale = languages ? languages[0] : userLanguage || language;
	return new Date(milliseconds).toLocaleDateString(userLocale);
};
const isPlural = (num: number) => num > 1;

export default (time: number): string => {
	const currentTimestamp = new Date().getTime();
	const msDifference = currentTimestamp - time;

	const seconds = toSeconds(msDifference);
	if (seconds < 60) {
		return `less than a minute ago`;
	}
	const minutes = toMinutes(msDifference);
	if (minutes < 60) {
		return `${minutes} ${isPlural(minutes) ? "minutes" : "minute"} ago`;
	}
	const hours = toHours(msDifference);
	if (hours < 24) {
		return `${hours} ${isPlural(hours) ? "hours" : "hour"} ago`;
	}
	const days = toDays(msDifference);
	if (days < 30) {
		return `${days} ${isPlural(days) ? "days" : "day"} ago`;
	}
	return toExactDate(time);
};
