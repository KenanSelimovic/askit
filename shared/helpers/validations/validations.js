// @flow
"use strict";

// just to make all validations importable from a single file

export const validateEmail = require("./emailValidation").default;
export const validateText = require("./textValidation").default;
export const validatePassword = require("./passwordValidation").default;
export const validateTimestamp = require("./timestampValidation").default;
export const validateDateOfBirth = require("./dateOfBirthValidation").default;
export const validateNumber = require("./numberValidation").default;
export const validateGender = require("./genderValidation").default;
