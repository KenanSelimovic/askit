// @flow
"use strict";

export default (timestamp: number, futureOnly: ?boolean): boolean => {
	if (!timestamp && timestamp !== 0) {
		return false;
	}
	if (new Date(timestamp).toString() === "Invalid Date") {
		return false;
	}
	if (futureOnly) {
		const currentDateTime = new Date().getTime();
		if (timestamp < currentDateTime) {
			return false;
		}
	}
	return true;
};
