// @flow
"use strict";

export default (gender: string): boolean => {
	return (gender === "female" || gender === "male");
};
