// @flow
"use strict";

const validateTimestamp = require("./timestampValidation");

export default (dateOfBirth: number, minAge: number = 10): boolean => {
	if (!validateTimestamp(dateOfBirth)) {
		return false;
	}
	let dateToCompareTo = new Date();
	dateToCompareTo.setFullYear(dateToCompareTo.getFullYear() - minAge);
	const fiveYearsAgoTimestamp = dateToCompareTo.getTime();
	if (fiveYearsAgoTimestamp < dateOfBirth) {
		return false;
	}
	return true;
};
