// @flow
"use strict";
import type {QuestionsStateType} from "../modules/questions/reducers/questionsReducer";
import type {ProfileInStateType} from "../modules/profile/reducers/profileReducer";
import type {AuthStateType} from "../modules/auth/reducers/authReducer";
import type {VotesInStateType} from "../modules/votes/reducers/votesReducer.flow";
import type {TopUsersStateType} from "../modules/questions/reducers/topUsersReducer";


export type GlobalAppStateType = {|
	ajaxCallsInProgress: number,
	auth: AuthStateType,
	profile: ProfileInStateType,
	questions: QuestionsStateType,
	topUsers: TopUsersStateType,
	votes: VotesInStateType
|};

export default {
	ajaxCallsInProgress: 0,
	auth: {
		token: null,
		id: null
	},
	profile: {
		data: null
	},
	questions: {
		hotQuestions: {
			data: []
		},
		latestQuestions: {
			data: [],
			totalPages: 1
		},
		myQuestions: {
			data: [],
			totalPages: 1
		},
		search: {
			data: [],
			totalPages: 1,
			searchText: ""
		}
	},
	topUsers: {
		data: []
	},
	votes: {
		userVotes: []
	}
};
