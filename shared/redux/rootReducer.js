// @flow
"use strict";
import {combineReducers} from "redux";
import auth from "../modules/auth/reducers/authReducer";
import profile from "../modules/profile/reducers/profileReducer";
import ajaxCallsInProgress from "../modules/layout/reducers/ajaxStatusReducer";
import questions from "../modules/questions/reducers/questionsReducer";
import topUsers from "../modules/questions/reducers/topUsersReducer";
import votes from "../modules/votes/reducers/votesReducer.flow";

export default combineReducers({
	ajaxCallsInProgress,
	auth,
	profile,
	questions,
	topUsers,
	votes
});
