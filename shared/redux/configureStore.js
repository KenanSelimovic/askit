// @flow
"use strict";
import {createStore, compose, applyMiddleware} from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import thunk from 'redux-thunk';
import rootReducer from './rootReducer';

export default (initialState: Object) => {
	// Middleware and store enhancers
	const enhancers = [
		applyMiddleware(thunk)
	];

	if (process.env.CLIENT && process.env.NODE_ENV !== "production") {
		// Enable DevTools only when rendering on client and during development.
		enhancers.push(
			window.devToolsExtension ? window.devToolsExtension() : f => f
		);
	}

	return createStore(rootReducer, initialState, compose(...enhancers));
};
