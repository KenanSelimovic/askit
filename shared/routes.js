// @flow
"use strict";
import React from "react";
import {Route, IndexRoute} from "react-router";
import App from "./modules/App";
import Register from "./modules/auth/reactComponents/Register";
import Login from "./modules/auth/reactComponents/Login";
import EditProfile from "./modules/profile/reactComponents/EditProfile";
import QuestionFull from "./modules/questions/reactComponents/QuestionFull";
import QuestionLists from "./modules/questions/reactComponents/QuestionLists";
import MyQuestions from "./modules/questions/reactComponents/MyQuestions";

export default (
	<Route path="/" component={App}>
		<IndexRoute component={QuestionLists} />
		<Route path="/register" component={Register} />
		<Route path="/login" component={Login} />
		<Route path="/profile" component={EditProfile} />
		<Route path="/question/:id" component={QuestionFull} />
		<Route path="/my-questions" component={MyQuestions} />
	</Route>
);
