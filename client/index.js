// @flow
"use strict";
import React from "react";
import {render} from "react-dom";
import {Provider} from "react-redux";
import configureStore from "../shared/redux/configureStore";
import {Router, match} from "react-router";
import routes from "../shared/routes";
import createBrowserHistory from 'history/lib/createBrowserHistory';
import "isomorphic-fetch";
import $ from "jquery";
import toastr from "toastr";

/*
* Handle redux stuff
*/
// this is from store.getState on server
const initialState = window.__INITIAL_STATE__;
// Create Redux store with initial state
const store = configureStore(initialState);


if (process.env.CLIENT) {
	// setup client libraries config
	window.$ = window.jQuery = $;
	toastr.options.closeButton = true;
	toastr.options.preventDuplicates = true;
	toastr.options.timeOut = 2000;
	toastr.options.closeDuration = 100;


	window.addEventListener("error", err => {
		console.log(err);
		require("./modules/logging/errorLogger").default(err);
	});

	/*
	 * Let react-router handle proper component,
	 * while wrapping everything in redux store provider
	 */
	match({history: createBrowserHistory(), routes}, (error, redirectLocation, renderProps) => {
		render(
			<Provider store={store}>
				<Router {...renderProps} />
			</Provider>,
			document.getElementById("root"));
	});
}
