// @flow
"use strict";
let JL = require('jsnlog').JL;
JL.setOptions({defaultAjaxUrl: "/logerror"});

// send error to the app backend, so that log file gets updated
export default (err: Error) => {
	if (typeof err === "object") {
		JL().fatalException("Exception info", err);
	}
	else {
		JL().error(err);
	}
};
