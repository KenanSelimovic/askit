FROM node:8
# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
# Install app dependencies
COPY package.json /usr/src/app/
RUN npm install
# Bundle app source
COPY . /usr/src/app
ARG MINIFY
RUN if [ "$MINIFY" = "true" ] ; then npm run build-production; else npm run build; fi
EXPOSE 4000
EXPOSE 3000
CMD [ "npm", "start" ]
