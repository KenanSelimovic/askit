// @flow
"use strict";
import "isomorphic-fetch";
import React from "react";
import ReactDOMServer from "react-dom/server";
import {Provider} from "react-redux";
import configureStore from "../shared/redux/configureStore";
import {match, RouterContext} from 'react-router';
import routes from "../shared/routes";
import renderFullPage from "./renderFullPage";
import visitingProtectedRoute from "./methods/visitingProtectedRoute";
import getProfile from "../shared/modules/profile/methods/getProfile";
import getLatestQuestions from "../shared/modules/questions/methods/fetchLatestQuestions";
import getHotQuestions from "../shared/modules/questions/methods/fetchHotQuestions";
import getMyQuestions from "../shared/modules/questions/methods/fetchMyQuestions";
import getInitialState from "./methods/getInitialState";
import {crashLogger} from "./modules/logging/logging";
import routeMatcher from "./methods/matchRoute";

import type {
	express$Request,
	express$Response,
	express$NextFunction
} from "express";

export default (req: express$Request, res: express$Response, next: express$NextFunction) => {
	let initialState, store, html;
	const matchRoute = routeMatcher(req.originalUrl);
	match({routes, location: req.originalUrl}, async (err, redirectLocation, renderProps) => {
		if (err) {
			res.status(500).end('Internal server error');
			return;
		}
		if (redirectLocation) {
			res.redirect(302, redirectLocation.pathname + redirectLocation.search);
			return;
		}

		if (!renderProps) {
			next();
			return;
		}

		function sendFinalData(initialData: Object) {
			store = configureStore(initialData);

			html = ReactDOMServer.renderToString(
				<Provider store={store}>
					<RouterContext {...renderProps} />
				</Provider>
			);
			initialState = store.getState();
			res.header("Content-Type", "text/html; charset=utf-8");
			res.status(200).end(renderFullPage(html, initialState));
		}

		/*
		* Handle data needed for each route
		*/
		let profileData = null
		try {
			profileData = await getProfile(req.cookies.askitToken);
		}
		catch (profileErr) {
			crashLogger.log(profileErr);
		}
		let hotQuestions = [];
		let latestQuestions = null;
		try {
			hotQuestions = await getHotQuestions();
			latestQuestions = await getLatestQuestions(1);
		}
		catch (questionsLoadError) {
			crashLogger.log(questionsLoadError);
		}
		const isAuthenticated = !!profileData;

		if (!isAuthenticated && visitingProtectedRoute(req.originalUrl)) {
			res.redirect("/login");
			return;
		}
		if (isAuthenticated && (matchRoute("/register") || matchRoute("/login"))) {
			res.redirect("/");
			return;
		}
		let myQuestions = null;
		if (matchRoute("/my-questions")) {
			myQuestions = await getMyQuestions(1, req.cookies.askitToken);
		}
		const initialData = getInitialState({
			cookies: req.cookies,
			profileData,
			latestQuestions,
			hotQuestions,
			myQuestions
		});
		sendFinalData(initialData);
	});
};
