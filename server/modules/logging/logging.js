// @flow
"use strict";
require('winston-daily-rotate-file');

import type {express$Application} from "express";

const loggingTypes = ["requestLogs", "developerErrors", "crashLogger"];
/**
  * Function that needs to be called to prepare logging module for future
  * logging. It's supposed to be called only once, on server startup
  */
const initialise = (app: express$Application) => {
	const fs = require("fs");
	if (!fs.existsSync("logs")) {
		fs.mkdirSync("logs", 0o774)
	}
	for(let dir of loggingTypes) {
		const path = "logs/" + dir;
		if (!fs.existsSync(path)) {
			fs.mkdirSync(path, 0o774)
		}
	}
	require("./requestLogs/requestLogs")(app);
};

const crashLogger = require("./crashLogger/crashLogger")();

module.exports.initialise = initialise;
module.exports.crashLogger = crashLogger;
