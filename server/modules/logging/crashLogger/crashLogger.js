// @flow
"use strict";
const winston = require('winston');
const WinstonLogger = winston.Logger;

// eslint-disable-next-line immutable/no-mutation
module.exports = (): WinstonLogger => {
	const crashLoggerTransport = new winston.transports.DailyRotateFile({
		filename: 'logs/crashLogger/crashes-',
		datePattern: 'yyyy-MM-dd',
		prepend: false,
		handleExceptions: true
	});
	return new (winston.Logger)({
		transports: [
			new (winston.transports.Console)(),
			crashLoggerTransport
		]
	});
};
