// @flow
"use strict";
const fs = require("fs");
const bundledJs = fs.readFileSync(__dirname + "/../dist/js/bundle.js");

// eslint-disable-next-line immutable/no-mutation
module.exports = (html: string, initialState: ReduxStateType): string => {
	return `
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<meta name="author" content="Kenan"/>
			<title>ask.it</title>
			<style>@import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500');</style>
			<link rel="stylesheet" href="/css/normalize.css"/>
			<link rel="stylesheet" href="/css/bootstrap.min.css"/>
			<link rel="stylesheet" href="/css/toastr.min.css"/>
			<link rel="stylesheet" href="/css/font-awesome.min.css"/>
			<link rel="stylesheet" href="/css/styles.css"/>
		</head>
		<body>
			<div id="root">${html}</div>
			<script src="/js/jquery-3.2.1.min.js"></script>
			<script src="/js/bootstrap.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
			<script>
                window.__INITIAL_STATE__ = ${JSON.stringify(initialState)}
            </script>
			<script>${bundledJs.toString()}</script>
		</body>
		</html>
	`;
};
