"use strict";
require("babel-register");
require("babel-polyfill");
const express = require("express");
const requestHandler = require("./requestHandler").default;
const cluster = require("cluster");
const helmet = require("helmet");
const cookieParser = require("cookie-parser");


const app = express();
const port = process.env.PORT || require("../shared/config").config.port;

if (cluster.isMaster) {
	const numWorkers = require('os').cpus().length;

	console.log('Master cluster setting up ' + numWorkers + ' workers...');

	for(let i = 0; i < numWorkers; i++) {
		cluster.fork();
	}

	cluster.on('online', (worker) => {
		console.log('Worker ' + worker.process.pid + ' is online');
	});

	cluster.on('exit', (worker, code, signal) => {
		cluster.fork();
	});
}
else {

	/*
	 * Setup security defaults
	 */
	app.use(helmet());

	/*
	 * Parse cookies
	 */
	app.use(cookieParser());

	/*
	 * Serve static files (needs to be change for production environement)
	 */
	app.use("/", express.static(__dirname + "/../dist"));

	/*
	* Setup logging
	*/
	require("./modules/logging/logging").initialise(app);

	/*
	* Handle special routes
	*/
	app.post("/logerror", require("body-parser").json(), req => {
		const {crashLogger} = require("./modules/logging/logging");
		crashLogger.log("error", req.body);
	});

	/*
	* Handle react-router routes
	*/
	app.use(requestHandler);

	/*
	* Finally, start listening
	*/
	app.listen(port);
}
