// @flow
"use strict";
// flow types
import type {GlobalAppStateType} from "../../shared/redux/defaultState";
import type {ProfileDataType} from "../../shared/modules/profile/flowTypes/profile.flow";
import type {QuestionType} from "../../shared/modules/questions/flowTypes/questions.flow";
import type {
	AuthStateType
} from "../../shared/modules/auth/reducers/authReducer";
export type UserCookiesType = {
	askitToken?: string,
	askitUserId?: string
};

const defaultState: GlobalAppStateType = require("../../shared/redux/defaultState").default;

const authenticateState = (
	unauthenticated: AuthStateType,
	token: string,
	userId: number
): AuthStateType => {
	return Object.assign(
		{},
		unauthenticated,
		{
			token,
			id: userId
		}
	);
};

// eslint-disable-next-line immutable/no-mutation
module.exports = (initialData: {|
	cookies: UserCookiesType,
	profileData: ?ProfileDataType,
	latestQuestions: ?{questions: Array<QuestionType>, page: number, total_pages: number},
	myQuestions: ?{questions: Array<QuestionType>, page: number, total_pages: number},
	hotQuestions: Array<QuestionType>
|}): GlobalAppStateType => {
	const {
		cookies,
		profileData,
		latestQuestions,
		hotQuestions,
		myQuestions
	} = initialData;
	const token = cookies.askitToken;
	const userId = parseInt(cookies.askitUserId, 10);
	return Object.assign(
		{},
		defaultState,
		{
			auth: token && profileData ? authenticateState(defaultState.auth, token, userId) : defaultState.auth,
			profile: Object.assign(
				{},
				defaultState.profile,
				{
					data: profileData ?
						{
							id: profileData.id || null,
							first_name: profileData.first_name || "",
							last_name: profileData.last_name || "",
							email: profileData.email || "",
							profile_image_url: profileData.profile_image_url || ""
						}
						: {}
				}
			),
			votes: Object.assign(
				{},
				defaultState.votes,
				{
					userVotes: profileData ? profileData.votes : []
				}
			),
			questions: Object.assign(
				{},
				defaultState.questions,
				{
					hotQuestions: {
						data: hotQuestions
					},
					latestQuestions: {
						data: latestQuestions ? latestQuestions.questions : [],
						totalPages: latestQuestions ? latestQuestions.total_pages : 1
					},
					myQuestions: {
						data: myQuestions ? myQuestions.questions : [],
						totalPages: myQuestions ? myQuestions.total_pages : 1
					}
				}
			)
		}
	);
};
