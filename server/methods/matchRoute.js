// @flow
"use strict";
export default (requestUrl: string) => {
	return (route: string) => requestUrl.indexOf(route) === 0;
};
