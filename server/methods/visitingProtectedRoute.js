// @flow
"use strict";
const protectedRoutes: Array<string> = [
	"/profile",
	"my-questions"
];

export default (originalUrl: string) => {
	for (const route of protectedRoutes) {
		if (originalUrl.indexOf(route) !== -1) {
			return true;
		}
	}
	return false;
};
